//class solution
class Solution{
public:
	//constructor
	//int objectiveNumber;
	//int decisionNumber;
	Problem problem;
	//__device__ __host__ Solution( int On, int Dn ) : problem(On, Dn), objectiveNumber(On), decisionNumber(Dn) {}

	//int objectiveNumber; //number of objectives
	//int decisionNumber; //number of decision variables
	double decisionVector[maxDecisionNumber]; //decision vector of this solution
	double objectiveVector[maxObjectiveNumber]; //objective vector of this solution
	double crowdingDistance;

	//double superiorPositionLimit;
	//double inferiorPositionLimit;

	double sigmaVector[maxSigmaSize];


	//compare two Solutions
	//param - the solution to be compared to
	__host__ bool isEqual(Solution sol);

	//randomly initialize a solution
	__device__ void initialize(str Pb, curandState* devStates);

	//evaluate this solution
	__device__ void evaluate();

	//correct the position if is beyond the bounds
	//__device__ bool truncatePositon();

	//keep the position in the swarm specified bounds in IMulti algorithm
	//__host__ __device__ bool truncatePositonIMulti(double* centroid, double range);

	//calculate the sigma vector of this solution
	__device__ void calculateSigmaVector(double* smallerPositions, double* largerPositions);

// 	//make a deep copy of a solution, not only pointing one solution to another
// 	//param - the solution to be copied from
// 	__device__ __host__ void deepCopy(Solution origin);
};
//#include "util.cpp"
//constructor initialize the variables
//Solution::Solution( int On, int Dn ){
//	objectiveNumber=On;
//	decisionNumber=Dn;
//	decisionVector= new double[decisionNumber];
//	objectiveVector= new double[objectiveNumber];
//}

__device__ __host__ double round_5(double in);
__host__ __device__ double combination(int m, int n);
__host__ __device__ double calculateSigma(double f1, double f2);
__host__ __device__ double normalize(double valor, double min, double max);


//comparator if two solutions are equal
//param - the solution to be compared to
__host__ bool Solution::isEqual(Solution sol){
	for(int i=0;i<objectiveNumber;i++){
		//if( sol.objectiveVector[i] != objectiveVector[i] ){
		if( round_5(sol.objectiveVector[i]) != round_5(objectiveVector[i]) ){
			return false;
		}
	}
	return true;
}

//randomly initialize a solution
__device__ void Solution::initialize(str Pb, curandState* devStates){
	//decisionVector= new double[decisionNumber];
	//objectiveVector= new double[objectiveNumber];

 	problem.problem=Pb;
	//sprintf(problem.problem, "%s",Pb);
	//problem.decisionNumber=decisionNumber;
	//problem.objectiveNumber=objectiveNumber;

	crowdingDistance = -1; //if the crowding distance is negative, it is not set

	int idx = blockIdx.x*blockDim.x + threadIdx.x;

	//if(leaders[idx%swarmNumber]==4){ //if the sigma method is used
//	if(leader==4){ //if the sigma method is used
		//int tamVector = (int) combination(objectiveNumber, 2);
		//sigmaVector = (double*)malloc(sizeof(double)*tamVector);
		//printf("%d\n",tamVector);
//	}

//	int idx = blockIdx.x*blockDim.x + threadIdx.x;
	curandState localState = devStates[idx];
	for(int i=0;i<d_decisionNumber;i++)
		//decisionVector[i]=(rand()/(double)RAND_MAX);
		//decisionVector[i]=curand_uniform( &localState);
		decisionVector[i]=d_inferiorPositionLimit[i]+ (curand_uniform( &localState)* (d_superiorPositionLimit[i]-d_inferiorPositionLimit[i]) );
   	devStates[idx] = localState;
	problem.evaluate(decisionVector, objectiveVector);
	//calculateSigmaVector(smallerPositions, largerPositions);
}
//evaluate the solution according to a predetermined problem
__device__ void Solution::evaluate(){
	problem.evaluate(decisionVector, objectiveVector);

	//truncate the solutions to 3 decimal cases
	//for(int i=0;i<objectiveNumber;i++)
	//	objectiveVector[i]=round_3(objectiveVector[i]);

	crowdingDistance= -1;
	memset(sigmaVector,0,sizeof(double)*sigmaSize);
}
// //make a deep copy of a solution, not only pointing one solution to another
// //param - the solution to be copied from
// __device__ __host__ void Solution::deepCopy(Solution origin){
// 
// 	problem.dtlz=origin.problem.dtlz;
// 
// 	decisionNumber=origin.decisionNumber;
// 	objectiveNumber=origin.objectiveNumber;
// 	
// 	sigmaSize=origin.sigmaSize;
// 
// 	crowdingDistance=origin.crowdingDistance;
// 
// 	memcpy(sigmaVector, origin.sigmaVector,sizeof(double)*sigmaSize);
// 
// 	memcpy(decisionVector, origin.decisionVector, sizeof(double)*decisionNumber);
// 	memcpy(objectiveVector, origin.objectiveVector, sizeof(double)*objectiveNumber);
// 
// }
// //correct the position if is beyond the bounds
// __device__ bool Solution::truncatePositon(){
// 	bool truncate=false;
// 	for(int i=0;i<decisionNumber;i++){
// 		double var_i=decisionVector[i];
// 		if(var_i<inferiorPositionLimit){
// 			double part1=(superiorPositionLimit-inferiorPositionLimit);
// 			double part2=abs(inferiorPositionLimit - var_i);
// 			double part3=fmod(part2,part1);
// 			double part4=superiorPositionLimit-part3;
// 
// 			decisionVector[i]=part4;
// 			truncate=true;
// 		}
// 		if(var_i>superiorPositionLimit){
// 			double part1=(superiorPositionLimit-inferiorPositionLimit);
// 			double part2=abs(var_i-superiorPositionLimit);
// 			double part3=fmod(part2,part1);
// 			double part4=part3+inferiorPositionLimit;
// 
// 			decisionVector[i]=part4;
// 			truncate=true;
// 		}
// 	}
// 	return truncate;
// }

__device__ void Solution::calculateSigmaVector(double* smallerPositions, double* largerPositions){
	//double sigmaVector[tamVector];
	int  cont = 0;
	for(int i = 0; i<d_objectiveNumber-1; i++){
		for(int j = i+1; j<d_objectiveNumber;j++){
			double obj1=normalize(objectiveVector[i], smallerPositions[i], largerPositions[i]);
			double obj2=normalize(objectiveVector[j], smallerPositions[j], largerPositions[j]);

			//double obj1 = objectiveVector[i];
			//double obj2 = objectiveVector[j];
			sigmaVector[cont++] = calculateSigma(obj1, obj2);
		}
	}
}
