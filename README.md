# Cuda MOPSO #

This project aim to evaluate differents MOPSO in MaOP using Cuda.

### How do I get set up? ###

export CUDA_HOME=/usr/local/cuda-5.5

export LD_LIBRARY_PATH=${CUDA_HOME}/lib64

PATH=${CUDA_HOME}/bin:${PATH}

export PATH

### Compile ###

cd cuda-pso

make

### Run ###

run-pso.sh

## How to access Hydra  ##

$ ssh username@200.17.212.91

## How to mount remote hydra filesystem ## 

sudo sshfs user@200.17.212.91:/ /mnt/hydra/
sudo nautilus /mnt/hydra
