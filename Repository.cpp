//class repository
class Repository{
public:

	//int objectiveNumber;
	//int decisionNumber;
	
	double smallerPositions[maxObjectiveNumber];
	double largerPositions[maxObjectiveNumber];

	str archiverType; //1-random,
	int repositorySize;//maximum particles stored in this repository before the filter is called
	
	bool archiverUsed; //flag to register if the repository was ever full
	
	int particlesEntered;//counts solutions that enters in each repository in each generation

	//initialize the main variables of the repository
	//params - the number of objectives
	//		 - the number of decision variables
	//		 - the maximum size of the repository
	//		 - the code of the archiver type selected (set in the begining of main.cpp)

	//Repository(int At, int Rs);
	__host__ __device__ void initialize(str At, int Rs);
	//tries to add a solution in the repository if it is non-dominated
	//param - the candidate solution to be inserted in the repository
	__host__  void add(Solution candidate);
	//after the update the repository gets messy (full of holes), this method make it continuous again.
	__host__ __device__ void organize();
	//safely get a solution from the repository
	//param - the index of the desired solution
	__host__ __device__ Solution getSolution(int index);
	//safely get the solutions from the repository
	__host__ __device__ Solution* getSolutions();
	//switch between the implemented archivers
	//param - the solution to be submitted to the archiver
	__host__ void archiver(Solution candidate);
	//archiver wich remove a random solution of the repository
	//param - the solution to be submitted to the archiver
	__host__ void randomArchiver(Solution candidate);
	//archiver wich consider the crowding distance to keep/exclude a solution
	//param - the solution to be submitted to the archiver
	__host__ void crowdingDistanceArchiver(Solution candidate);
	//archiver proposed by Carvalho and Pozo //Removes the solutio with worst distance to the ideal point
	//param - the solution to be submitted to the archiver
	__host__ void idealArchiver(Solution candidate);
	//archiver proposed by Laumanns and Zenklusken
	//param - the solution to be submitted to the archiver
	__host__ void MGAArchiver(Solution candidate);
	//get the actual size of the repository
	__host__ __device__ unsigned int getActualSize(){return actualSize;}
	//clear the repository
	__host__ __device__ void clear();
	//add a solution ignoring whether the repository is full or not
	__host__ void forceAdd(Solution candidate);

private:
	Solution solutions[maxRepositorySize+1];
	int actualSize; //actual number of solutions in the repository
	bool controlSolutions[maxRepositorySize+1];//control if one position have or not solutions associated
	//insert a solution in the repository with no verification
	//param - the candidate solution to be inserted in the repository (no verification)
	__host__ __device__ void insert(Solution solution);
	//exclude a solution of the repository using its real index
	//param - the index of the solution to be excluded from the repository (no verification)
	__host__ __device__ void exclude(int index);

	//control variables to paralelize the repository
	//int simultaneousReads;
	//int lock; //1=true, 0=false
	//atomicAdd(&simultaneousReads, 1);
	//atomicSub(&simultaneousReads, 1);

};
//constructor initialize the variables
/*Repository::Repository(int At, int Rs){
	repositorySize=Rs; //maximum particles stored in this repository before the filter is called
	actualSize=0; //the repository initialize with 0 solutions
	archiverType=At; //archiver type
	memset(controlSolutions, false, sizeof(bool)*(repositorySize+1));
}*/

__device__ __host__ void printVector(double* vec, int vecSize);
__device__ __host__ int dominance(double sol1[], double sol2[], int objectiveNumber);
__host__ __device__ bool isEqual(double* vec1, double* vec2, int size);
void obtainIdealSolution(Solution* solutions, Solution ideal, int objectiveNumber, int repActualSize);
__host__ int compute_b_mga(Solution* solutions, int repActualSize, double* smallerPositions, double* largerPositions);
__host__ void box_mga(Solution solution, double* output, int b, int objectiveNumber, double* smallerPositions, double* largerPositions);
__host__ void updateCrowdingDistances(Solution* solutions, int solutionNumber);
__host__ __device__ double normalize(double valor, double min, double max);

//initialize the main variables of the class
__host__ __device__ void Repository::initialize(str At, int Rs){
	//objectiveNumber=On;
	//decisionNumber=Dn;
	repositorySize=Rs; //maximum particles stored in this repository before the filter is called
	actualSize=0; //the repository initialize with 0 solutions
	archiverUsed=false;
	//sprintf(archiverType,"%s",At); //archiver type
	archiverType=At;
	memset(controlSolutions, false, sizeof(bool)*(maxRepositorySize+1));

	//simultaneousReads=0;
	//lock=-1;

	//controlSolutions=new bool[repositorySize];
	//solutions = new Solution[repositorySize];
	//for(int i=0;i<repositorySize;i++){
//		solutions[i].decisionVector = new double[decisionNumber];
//		solutions[i].objectiveVector = new double[objectiveNumber];
	//}
}

//tries to add a solution in the repository if it is non-dominated
//param - the candidate solution to be inserted in the repository
__host__  void Repository::add(Solution candidate){
	bool isDominated=false;
	bool equal=false;
	if(actualSize==0){ //if the repository is empty, insert the solution
		insert(candidate);
	}else{
		for(int s=0;s<repositorySize+1;s++){
			if(controlSolutions[s]){//if this solution is valid
				if(!candidate.isEqual(getSolution(s))){ //if the solutions are not equal
					//verify the dominance relation between two vectors
					//return 1 if sol1 dominates sol2, -1 if sol2 dominates sol1, 0 if they do not dominate each other, 2 if they are equal
					int dom=dominance(candidate.objectiveVector, getSolution(s).objectiveVector, objectiveNumber);
					if(dom == 1){//if the candidate dominates the solution in the repository
						exclude(s);
					}else{
						if(dom == -1){//if a solution in the repository dominates the candidate
							isDominated=true;
							break;
						}
					}
				}else{ //if the solutions are equal, discard the candidate
					equal=true;
					break;
				}
			}
		}

		if(!isDominated && !equal){ //if the solution is non-dominated
			if(actualSize<repositorySize){//if the repository is not empty nor full
					insert(candidate);//insert the solution
			}else{ //if the repository is full
				archiver(candidate);
			}
		}
	}
}
//insert a solution in the repository with no verification
//param - the candidate solution to be inserted in the repository (no verification)
__host__ __device__ void Repository::insert(Solution solution){
/*	int idx = blockIdx.x*blockDim.x + threadIdx.x;
	while(true){ //infinite loop
		if(lock == -1){ // when release all enter at once
			//atomicAdd(&lock, 1+idx);
			atomicExch(&lock, idx);
		}
		//printf("insert(lock1) - %d, %d - %d break\n", simultaneousReads, lock, idx);

		if(lock == idx){ //if i am the one which writes last, breaks the infinite loop
			printf("insert(lock2) - %d, %d - %d break\n", simultaneousReads, lock, idx);
			break;
		}
	}

	while(simultaneousReads){ //keep the read operation waiting until the write finishes
		//sleep(0.001);
		printf("insert(read) - %d, %d\n", simultaneousReads, lock);
	}*/
	//for(int s=0;s<repositorySize+1;s++){
	for(int s=0;s<repositorySize+1;s++){
		if(!controlSolutions[s]){//if the solution is invalid
			//solutions[s].deepCopy(solution);
			memcpy(&solutions[s], &solution, sizeof(Solution));
			controlSolutions[s]=true; //the solution is now valid
			actualSize++;
			particlesEntered++;
			//break;
			return;
		}
	}
	printf("\nERROR INSERTING SOLUTION!!!");
	//atomicSub(&lock, 1+idx);
	//atomicExch(&lock, -1);
}
//exclude a solution of the repository using its real index
//param - the index of the solution to be excluded from the repository (no verification)
__host__ __device__ void Repository::exclude(int index){
	//free(solutions[index].decisionVector);
	/*int idx = blockIdx.x*blockDim.x + threadIdx.x;
	while(true){ //infinite loop
		printf("exclude\n");
		if(lock == -1){ // when release all enter at once
			if(lock > idx) // all tries to set at once, but the smaller can do this
				atomicAdd(&lock, 1+idx);
		}
		if(lock == idx){ //if i am the smaller, breaks the infinite loop
			printf("exclude(lock) - %d, %d - %d break\n", simultaneousReads, lock, idx);
			break;
		}
	}
	while(simultaneousReads){ //keep the read operation waiting until the write finishes
		//sleep(0.001);
		printf("exclude(read) - %d, %d\n", simultaneousReads, lock);
	}*/

	if(controlSolutions[index]){//if the solution is valid
		controlSolutions[index]=false; //the solution can be overriten
		actualSize--;
	}else{
		printf("ERROR! TRYING TO REMOVE AN INVALID SOLUTION (%d).\n", index);
	}

	//atomicSub(&lock, 1+idx);
}
//after the update the repository gets messy (full of holes), this method make it continuous again.
__host__ __device__ void Repository::organize(){
	//while(lock != 0){

	//}
	//atomicAdd(&lock, 1);
	//while(simultaneousReads){ //keep the read operation waiting until the write finishes
		//sleep(0.001);
	//}

	for(int i=0;i<actualSize;i++){ // verify the valid solutions
		if(!controlSolutions[i]){//if the solution is invalid
			for(int s=actualSize;s<maxRepositorySize+1;s++){ // verify the solutions in the positions they shouldnt be
				if(controlSolutions[s]){//if the solution is valid
					solutions[i]=solutions[s];
					controlSolutions[i]=true;
					controlSolutions[s]=false;
					break;
				}
			}

		}
	}
	//atomicSub(&lock, 1);
}
//safely get a solution from the repository
//param - the index of the desired solution
__host__ __device__ Solution Repository::getSolution(int index){
	/*while(lock != -1){ //keep the read operation waiting until the write finishes
		//sleep(0.001);
		printf("getSolution - %d, %d\n", simultaneousReads, lock);
	}
	atomicAdd(&simultaneousReads, 1);*/

	if(!controlSolutions[index]){
		printf("ERROR, TRYING TO GET AN INVALID SOLUTION FROM THE REPOSITORY\n");
	}
	Solution sol = solutions[index];

	//atomicSub(&simultaneousReads, 1);
	return sol;
}

//safely the solutions from the repository
__host__ __device__ Solution* Repository::getSolutions(){
	/*while(lock != -1){ //keep the read operation waiting until the write finishes
		//sleep(0.001);
		printf("getSolutionSS - %d, %d\n", simultaneousReads, lock);
	}
	atomicAdd(&simultaneousReads, 1);*/

	//at this point the solutions are organized
	Solution* sol= solutions;

	//atomicSub(&simultaneousReads, 1);
	return sol;
}
//switch between the implemented archivers
//param - the solution to be submitted to the archiver
__host__  void Repository::archiver(Solution candidate){
	archiverUsed=true;
	if(!strncmp(archiverType.value, "rnd", 3))
		randomArchiver(candidate);
	if(!strncmp(archiverType.value, "cd", 2))
		crowdingDistanceArchiver(candidate);
	if(!strncmp(archiverType.value, "ideal", 5))
		idealArchiver(candidate);
	if(!strncmp(archiverType.value, "mga", 3))
		MGAArchiver(candidate);
	
	if(strncmp(archiverType.value, "rnd", 3) && strncmp(archiverType.value, "cd", 2) && strncmp(archiverType.value, "ideal", 5) && strncmp(archiverType.value, "mga", 3))
		printf("INVALID ARCHIVER!\n");
	
// 	switch(archiverType){
// 		case 1:
// 			randomArchiver(candidate);
// 			break;
// 		case 2:
// 			crowdingDistanceArchiver(candidate);
// 			break;
// 		case 3:
// 			idealArchiver(candidate);
// 			break;
// 		case 4:
// 			MGAArchiver(candidate);
// 			break;
// 		default:
// 			printf("INVALID ARCHIVER!\n");
// 			break;
// 			//exit(1);
// 		}
}
//archiver wich remove a random solution of the repository
//param - the solution to be submitted to the archiver
__host__ void Repository::randomArchiver(Solution candidate){
	/*int idx = blockIdx.x*blockDim.x + threadIdx.x;
	curandState localState = devStates[idx];
	while(actualSize == repositorySize)
		exclude(((int)(curand_uniform( &localState)*actualSize)) % repositorySize);
	devStates[idx] = localState;*/
	while(actualSize >= repositorySize)
		exclude(rand() % repositorySize);
	insert(candidate);
}
//archiver proposed by Carvalho and Pozo //Removes the solutio with worst distance to the ideal point
//param - the solution to be submitted to the archiver
__host__ void Repository::idealArchiver(Solution candidate){
	//Obtains the ideal solution from the current front
// 	Solution ideal;
// 	for(int i=0;i<objectiveNumber;i++)
// 		ideal.objectiveVector[i]=MAXDOUBLE;
	
	//obtainIdealSolution(solutions, ideal, objectiveNumber, actualSize);

	insert(candidate);
	organize();
	//For each solution on the front, it calculates the distance to the ideal point
	double smallerDistance[actualSize];
	
	for(int i=0;i<actualSize;i++){
		double norm[objectiveNumber];
		double sp_norm[objectiveNumber];
		for(int j=0;j<objectiveNumber;j++){
			norm[j]=normalize(solutions[i].objectiveVector[j],smallerPositions[j],largerPositions[j]);
			sp_norm[j]=normalize(smallerPositions[j],smallerPositions[j],largerPositions[j]);
		}
		smallerDistance[i] = calculateEuclideanDistance(sp_norm, norm, objectiveNumber);
		//smallerDistance[i] = calculateEuclideanDistance(smallerPositions, solutions[i].objectiveVector, objectiveNumber);
	}

	double highDistanceValue = -1.0;
	int index = -1;
	for (int i = 0; i<actualSize; i++) {
		if(smallerDistance[i] > highDistanceValue){
			highDistanceValue =smallerDistance[i];
			index = i;
		}
	}
	if(index==-1){
		printf("\nIDEAL ARCHIVER ERROR %f\n", highDistanceValue);
		exit(1);
	}
	exclude(index);
}
//archiver proposed by Laumanns and Zenklusken
//param - the solution to be submitted to the archiver
__host__ void Repository::MGAArchiver(Solution candidate){
	insert(candidate);
	organize();
	int b = compute_b_mga(solutions, actualSize, smallerPositions, largerPositions);
	int index_removed = -1;
	while(index_removed==-1){
		for(int i = actualSize-1; i>=0;i--){
			double box_i[objectiveNumber];
			box_mga(solutions[i], box_i, b, objectiveNumber, smallerPositions, largerPositions);
			for(int j = actualSize-1; j>=0;j--){
				if(i!=j){
					double box_j[objectiveNumber];
					box_mga(solutions[j], box_j, b, objectiveNumber, smallerPositions, largerPositions);
					int comparation = dominance(box_i, box_j, objectiveNumber);
					if(comparation == -1 || isEqual(box_i, box_j, objectiveNumber) ){
						index_removed = i;
						break;
					}
				}
			}
			//if(index_removed!=-1){
			//	break;
			//}
		}
		b--;
	}
	exclude(index_removed);
}

//archiver wich consider the crowding distance to keep/exclude a solution
//param - the solution to be submitted to the archiver
__host__ void Repository::crowdingDistanceArchiver(Solution candidate){
	//Solution temp[repositorySize+1]; //create a temporary repository to contain all the solutions plus the candidate
	double smallerCrowdingDistance=MAXDOUBLE;
	int idxSmallerCrowdingDistance=-1;

	//sync(temp); //sincronize the new repository with the solutions already found
//	for(int i=0;i<actualSize;i++)
//		temp[i]=solutions[i];
	//memcpy(temp, solutions, sizeof(Solution)*actualSize);

	//solutions[actualSize]=candidate;//insert the new one
	insert(candidate);
		
	organize();
	updateCrowdingDistances(solutions, actualSize); //update the crowing distances

	for(int i=0;i<actualSize;i++){//find the valid solution with smallest crowding distance
		if(controlSolutions[i] && getSolution(i).crowdingDistance<smallerCrowdingDistance){
			smallerCrowdingDistance=getSolution(i).crowdingDistance;
			idxSmallerCrowdingDistance=i;
		}
	}
//	if(!solutions[idxSmallerCrowdingDistance].isEqual(candidate)){ //if the candidate is not the solution with smallest crowding distance
		exclude(idxSmallerCrowdingDistance); //remove the solution with the smallest crowding distance
		//insert(candidate);//insert the new solution
//	}

	//free(temp);
}

//clear the repository
__host__ __device__ void Repository::clear(){
	actualSize=0; //the repository initialize with 0 solutions
	archiverUsed=false;
	memset(controlSolutions, false, sizeof(bool)*(maxRepositorySize+1));
}

//add a solution ignoring whether the repository is full or not
__host__ void Repository::forceAdd(Solution candidate){
	bool isDominated=false;
	bool equal=false;
	if(actualSize==0){ //if the repository is empty, insert the solution
		insert(candidate);
	}else{
		for(int s=0;s<repositorySize+1;s++){
			if(controlSolutions[s]){//if this solution is valid
				if(!candidate.isEqual(getSolution(s))){ //if the solutions are not equal
					//verify the dominance relation between two vectors
					//return 1 if sol1 dominates sol2, -1 if sol2 dominates sol1, 0 if they do not dominate each other, 2 if they are equal
					if(dominance(candidate.objectiveVector, getSolution(s).objectiveVector, objectiveNumber) == 1){//if the candidate dominates the solution in the repository
						exclude(s);
					}else{
						if(dominance(candidate.objectiveVector, getSolution(s).objectiveVector, objectiveNumber) == -1){//if a solution in the repository dominates the candidate
							isDominated=true;
							break;
						}
					}
				}else{ //if the solutions are equal, discard the candidate
					equal=true;
					break;
				}
			}
		}

		if(!isDominated && !equal){ //if the solution is non-dominated
			if(actualSize<maxRepositorySize){//if there is memory left
					insert(candidate);//insert the solution
			}else{ //if there is not memory left
				printf("REPOSITORY MEMORY UNAVAILABLE, INCREASE THE REPOSITORY MAXIMUM SIZE\n");
				//exit(1);
			}
		}
	}
}
/****************************************************************************************************************************************************************************************************/

//class repositoryFinal
class RepositoryFinal{
public:
	int repositorySize;//maximum particles stored in this repository before the filter is called

	__host__ __device__ void initialize();
	//tries to add a solution in the repository if it is non-dominated
	//param - the candidate solution to be inserted in the repository
	__host__  void add(Solution candidate);
	//insert the solutions from an entire repository into the final repository
	//param - the repository to be inserted
	__host__  void add(Repository rep);
	//after the update the repository gets messy (full of holes), this method make it continuous again.
	__host__ __device__ void organize();
	//safely get a solution from the repository
	//param - the index of the desired solution
	__host__ __device__ Solution getSolution(int index);
	//safely get the solutions from the repository
	__host__ __device__ Solution* getSolutions();
	//get the actual size of the repository
	__host__ __device__ int getActualSize(){return actualSize;}
	//clear the repository
	__host__ __device__ void clear();
	//add a solution ignoring whether the repository is full or not

private:
	Solution solutions[(maxRepositorySize)*maxSwarmNumber];
	int actualSize; //actual number of solutions in the repository
	bool controlSolutions[(maxRepositorySize)*maxSwarmNumber];//control if one position have or not solutions associated
	//insert a solution in the repository with no verification
	//param - the candidate solution to be inserted in the repository (no verification)
	__host__ __device__ void insert(Solution solution);
	//exclude a solution of the repository using its real index
	//param - the index of the solution to be excluded from the repository (no verification)
	__host__ __device__ void exclude(int index);
};
//initialize the main variables of the class
__host__ __device__ void RepositoryFinal::initialize(){
	repositorySize=(maxRepositorySize)*maxSwarmNumber; //maximum particles stored in this repository before the filter is called
	actualSize=0; //the repository initialize with 0 solutions
	memset(controlSolutions, false, sizeof(bool)*((maxRepositorySize)*maxSwarmNumber));
}

//tries to add a solution in the repository if it is non-dominated
//param - the candidate solution to be inserted in the repository
__host__  void RepositoryFinal::add(Solution candidate){
		bool isDominated=false;
		bool equal=false;
		if(actualSize==0){ //if the repository is empty, insert the solution
			insert(candidate);
		}else{
			for(int s=0;s<repositorySize;s++){
				if(controlSolutions[s]){//if this solution is valid
					if(!candidate.isEqual(getSolution(s))){ //if the solutions are not equal
						//verify the dominance relation between two vectors
						//return 1 if sol1 dominates sol2, -1 if sol2 dominates sol1, 0 if they do not dominate each other, 2 if they are equal
						int dom=dominance(candidate.objectiveVector, getSolution(s).objectiveVector, objectiveNumber);
						if(dom == 1){//if the candidate dominates the solution in the repository
							exclude(s);
						}else{
							if(dom == -1){//if a solution in the repository dominates the candidate
								isDominated=true;
								break;
							}
						}
					}else{ //if the solutions are equal, discard the candidate
						equal=true;
						break;
					}
				}
			}

			if(!isDominated && !equal){ //if the solution is non-dominated
				if(actualSize<repositorySize){//if the repository is not empty nor full
						insert(candidate);//insert the solution
				}else{ //if the repository is full
					//archiver(candidate);
					printf("ERROR! THE FINAL REPOSITORY IS FULL");
					exit(1);
				}
			}
		}
}
//insert the solutions from an entire repository into the final repository
//param - the repository to be inserted
__host__  void RepositoryFinal::add(Repository rep){
	for(int p=0;p<rep.getActualSize();p++){
		add(rep.getSolution(p));
	}
}
//insert a solution in the repository with no verification
//param - the candidate solution to be inserted in the repository (no verification)
__host__ __device__ void RepositoryFinal::insert(Solution solution){
	for(int s=0;s<(maxRepositorySize)*maxSwarmNumber;s++){
		if(!controlSolutions[s]){//if the solution is invalid
			//solutions[s].deepCopy(solution);
			memcpy(&solutions[s], &solution, sizeof(Solution));
			controlSolutions[s]=true; //the solution is now valid
			actualSize++;
			//break;
			return;
		}
	}
	printf("\nERROR INSERTING SOLUTION!!!");
}
//exclude a solution of the repository using its real index
//param - the index of the solution to be excluded from the repository (no verification)
__host__ __device__ void RepositoryFinal::exclude(int index){
	if(controlSolutions[index]){//if the solution is valid
		controlSolutions[index]=false; //the solution can be overriten
		actualSize--;
	}else{
		printf("ERROR! TRYING TO REMOVE AN INVALID SOLUTION (%d).\n", index);
	}
}
//after the update the repository gets messy (full of holes), this method make it continuous again.
__host__ __device__ void RepositoryFinal::organize(){
	for(int i=0;i<actualSize;i++){ // verify the valid solutions
		if(!controlSolutions[i]){//if the solution is invalid
			for(int s=actualSize;s<(maxRepositorySize)*maxSwarmNumber;s++){ // verify the solutions in the positions they shouldnt be
				if(controlSolutions[s]){//if the solution is valid
					solutions[i]=solutions[s];
					controlSolutions[i]=true;
					controlSolutions[s]=false;
					break;
				}
			}

		}
	}
}
//safely get a solution from the repository
//param - the index of the desired solution
__host__ __device__ Solution RepositoryFinal::getSolution(int index){
	if(!controlSolutions[index]){
		printf("ERROR, TRYING TO GET AN INVALID SOLUTION FROM THE FINAL REPOSITORY\n");
	}
	Solution sol = solutions[index];
	return sol;
}

//safely get the solutions from the repository
__host__ __device__ Solution* RepositoryFinal::getSolutions(){
	//at this point the solutions are organized
	Solution* sol= solutions;
	return sol;
}
//clear the repository
__host__ __device__ void RepositoryFinal::clear(){
	actualSize=0; //the repository initialize with 0 solutions
	memset(controlSolutions, false, sizeof(bool)*((maxRepositorySize)*maxSwarmNumber));
}