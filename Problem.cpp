class Problem{
public:
	//constructor
	//objective number, dimension number
	//Problem(int On, int Dn);

	//int objectiveNumber; //number of objectives
	//int decisionNumber; //number of decision variables
	//double* decisionVector;
	//double* objectiveVector;

	str problem;

	//swich between the problems implemented and evaluate the solution with the chosen
	//params - the decision vector to be evaluated
	//		 - the objective vector to hold the evaluation values
	__device__ void evaluate(double* decisionVector, double* objectiveVector);
	
	//function used in the WFG problems
	//params - the decision vector to be evaluated
	__device__ void calculate_x(double* t);

	//evaluate a solution using the DTLZ1 problem
	//params - the decision vector to be evaluated
	//		 - the objective vector to hold the evaluation values
	__device__ void calculateDTLZ1(double* decisionVector, double* objectiveVector);
	//evaluate a solution using the DTLZ2 problem
	//params - the decision vector to be evaluated
	//		 - the objective vector to hold the evaluation values
	__device__ void calculateDTLZ2(double* decisionVector, double* objectiveVector);
	//evaluate a solution using the DTLZ3 problem
	//params - the decision vector to be evaluated
	//		 - the objective vector to hold the evaluation values
	__device__ void calculateDTLZ3(double* decisionVector, double* objectiveVector);
	//evaluate a solution using the DTLZ4 problem
	//params - the decision vector to be evaluated
	//		 - the objective vector to hold the evaluation values
	__device__ void calculateDTLZ4(double* decisionVector, double* objectiveVector);
	//evaluate a solution using the DTLZ5 problem
	//params - the decision vector to be evaluated
	//		 - the objective vector to hold the evaluation values
	__device__ void calculateDTLZ5(double* decisionVector, double* objectiveVector);
	//evaluate a solution using the DTLZ6 problem
	//params - the decision vector to be evaluated
	//		 - the objective vector to hold the evaluation values
	__device__ void calculateDTLZ6(double* decisionVector, double* objectiveVector);
	//evaluate a solution using the DTLZ7 problem
	//params - the decision vector to be evaluated
	//		 - the objective vector to hold the evaluation values
	__device__ void calculateDTLZ7(double* decisionVector, double* objectiveVector);
	//evaluate a solution using the WFG1 problem
	//params - the decision vector to be evaluated
	//		 - the objective vector to hold the evaluation values
	__device__ void calculateWFG1(double* decisionVector, double* objectiveVector);
	//evaluate a solution using the WFG2 problem
	//params - the decision vector to be evaluated
	//		 - the objective vector to hold the evaluation values
	__device__ void calculateWFG2(double* decisionVector, double* objectiveVector);
	//evaluate a solution using the WFG3 problem
	//params - the decision vector to be evaluated
	//		 - the objective vector to hold the evaluation values
	__device__ void calculateWFG3(double* decisionVector, double* objectiveVector);
	//evaluate a solution using the WFG4 problem
	//params - the decision vector to be evaluated
	//		 - the objective vector to hold the evaluation values
	__device__ void calculateWFG4(double* decisionVector, double* objectiveVector);
	//evaluate a solution using the WFG5 problem
	//params - the decision vector to be evaluated
	//		 - the objective vector to hold the evaluation values
	__device__ void calculateWFG5(double* decisionVector, double* objectiveVector);
	//evaluate a solution using the WFG6 problem
	//params - the decision vector to be evaluated
	//		 - the objective vector to hold the evaluation values
	__device__ void calculateWFG6(double* decisionVector, double* objectiveVector);
	//evaluate a solution using the WFG7 problem
	//params - the decision vector to be evaluated
	//		 - the objective vector to hold the evaluation values
	__device__ void calculateWFG7(double* decisionVector, double* objectiveVector);
	//evaluate a solution using the WFG8 problem
	//params - the decision vector to be evaluated
	//		 - the objective vector to hold the evaluation values
	__device__ void calculateWFG8(double* decisionVector, double* objectiveVector);
	//evaluate a solution using the WFG9 problem
	//params - the decision vector to be evaluated
	//		 - the objective vector to hold the evaluation values
	__device__ void calculateWFG9(double* decisionVector, double* objectiveVector);

};


__device__ __host__ void printVector(double* vec, int vecSize);




//constructor initialize the variables
/*Problem::Problem(int On, int Dn){
	objectiveNumber=On;
	decisionNumber=Dn;
	//decisionVector=solution.decisionVector;
}*/

//swich between the problems implemented and evaluate the solution with the chosen
//params - the decision vector to be evaluated
//		 - the objective vector to hold the evaluation values
__device__ int cudaStrncmp(char* str1, char* str2, int size);
__device__ void Problem::evaluate(double* decisionVector, double* objectiveVector){
	
	if(!cudaStrncmp(problem.value, "dtlz1", 5))
		calculateDTLZ1(decisionVector, objectiveVector);
	if(!cudaStrncmp(problem.value, "dtlz2", 5))
		calculateDTLZ2(decisionVector, objectiveVector);
	if(!cudaStrncmp(problem.value, "dtlz3", 5))
		calculateDTLZ3(decisionVector, objectiveVector);
	if(!cudaStrncmp(problem.value, "dtlz4", 5))
		calculateDTLZ4(decisionVector, objectiveVector);
	if(!cudaStrncmp(problem.value, "dtlz5", 5))
		calculateDTLZ5(decisionVector, objectiveVector);
	if(!cudaStrncmp(problem.value, "dtlz6", 5))
		calculateDTLZ6(decisionVector, objectiveVector);
	if(!cudaStrncmp(problem.value, "dtlz7", 5))
		calculateDTLZ7(decisionVector, objectiveVector);
	if(!cudaStrncmp(problem.value, "wfg1", 4))
		calculateWFG1(decisionVector, objectiveVector);
	if(!cudaStrncmp(problem.value, "wfg2", 4))
		calculateWFG2(decisionVector, objectiveVector);
	if(!cudaStrncmp(problem.value, "wfg3", 4))
		calculateWFG3(decisionVector, objectiveVector);
	if(!cudaStrncmp(problem.value, "wfg4", 4))
		calculateWFG4(decisionVector, objectiveVector);
	if(!cudaStrncmp(problem.value, "wfg5", 4))
		calculateWFG5(decisionVector, objectiveVector);
	if(!cudaStrncmp(problem.value, "wfg6", 4))
		calculateWFG5(decisionVector, objectiveVector);
	if(!cudaStrncmp(problem.value, "wfg7", 4))
		calculateWFG5(decisionVector, objectiveVector);
	if(!cudaStrncmp(problem.value, "wfg8", 4))
		calculateWFG5(decisionVector, objectiveVector);
	if(!cudaStrncmp(problem.value, "wfg9", 4))
		calculateWFG5(decisionVector, objectiveVector);
	
	if(cudaStrncmp(problem.value, "dtlz1", 5) && cudaStrncmp(problem.value, "dtlz2", 5) && cudaStrncmp(problem.value, "dtlz3", 5) && cudaStrncmp(problem.value, "dtlz4", 5) && 
	   cudaStrncmp(problem.value, "dtlz5", 5) && cudaStrncmp(problem.value, "dtlz6", 5) && cudaStrncmp(problem.value, "dtlz7", 5) && cudaStrncmp(problem.value, "wfg1", 4) && 
	   cudaStrncmp(problem.value, "wfg2", 4) && cudaStrncmp(problem.value, "wfg3", 4) && cudaStrncmp(problem.value, "wfg4", 4) && cudaStrncmp(problem.value, "wfg5", 4) &&
	   cudaStrncmp(problem.value, "wfg6", 4) && cudaStrncmp(problem.value, "wfg7", 4) && cudaStrncmp(problem.value, "wfg8", 4) && cudaStrncmp(problem.value, "wfg9", 4) )
		printf("INVALID OPTIMIZATION PROBLEM! (%s)\n", problem.value);
}
//evaluate a solution using the DTLZ1 problem
//params - the decision vector to be evaluated
//		 - the objective vector to hold the evaluation values
__device__ void Problem::calculateDTLZ1(double* decisionVector, double* objectiveVector){

	int k= d_decisionNumber - d_objectiveNumber + 1;
	double g=0.0;

	for(int i=d_decisionNumber-k;i<d_decisionNumber;i++)
		g+=(decisionVector[i]-0.5)*(decisionVector[i]-0.5)-cos(20.0*M_PI*(decisionVector[i]-0.5));

	g=100*(k+g);
	for(int i=0;i<d_objectiveNumber;i++)
		objectiveVector[i] = (1.0+g)*0.5;

	for(int i=0;i<d_objectiveNumber;i++){
		for(int j=0;j<d_objectiveNumber-(i+1);j++)
			objectiveVector[i] *= decisionVector[j];
			if(i != 0){
				int aux = d_objectiveNumber - (i+1);
				objectiveVector[i] *= 1-decisionVector[aux] ;
			}
	}
}
//evaluate a solution using the DTLZ2 problem
//params - the decision vector to be evaluated
//		 - the objective vector to hold the evaluation values
__device__ void Problem::calculateDTLZ2(double* decisionVector, double* objectiveVector){

	int k= d_decisionNumber - d_objectiveNumber + 1;
	double g=0.0;

	for(int i=d_decisionNumber-k;i<d_decisionNumber;i++)
		g+=(decisionVector[i]-0.5)*(decisionVector[i]-0.5);

	for(int i=0;i<d_objectiveNumber;i++)
		objectiveVector[i] = (1.0+g);

	for(int i=0;i<d_objectiveNumber;i++){
		for(int j=0;j<d_objectiveNumber-(i+1);j++)
			objectiveVector[i] *= cos(decisionVector[j]*0.5*M_PI);
			if(i != 0){
				int aux = d_objectiveNumber - (i+1);
				objectiveVector[i] *= sin(decisionVector[aux]*0.5*M_PI);
			}
	}
}
//evaluate a solution using the DTLZ3 problem
//params - the decision vector to be evaluated
//		 - the objective vector to hold the evaluation values
__device__ void Problem::calculateDTLZ3(double* decisionVector, double* objectiveVector){

	int k= d_decisionNumber - d_objectiveNumber + 1;
	double g=0.0;

	for(int i=d_decisionNumber-k;i<d_decisionNumber;i++)
		g+=(decisionVector[i]-0.5)*(decisionVector[i]-0.5)-cos(20.0*M_PI*(decisionVector[i]-0.5));

	g=100.0*(k+g);
	for(int i=0;i<d_objectiveNumber;i++)
		objectiveVector[i] = 1.0+g;

	for(int i=0;i<d_objectiveNumber;i++){
		for(int j=0;j<d_objectiveNumber-(i+1);j++)
			objectiveVector[i] *= cos(decisionVector[j]*0.5*M_PI);
			if(i != 0){
				int aux = d_objectiveNumber - (i+1);
				objectiveVector[i] *= sin(decisionVector[aux]*0.5*M_PI);
			}
	}
}
//evaluate a solution using the DTLZ4 problem
//params - the decision vector to be evaluated
//		 - the objective vector to hold the evaluation values
__device__ void Problem::calculateDTLZ4(double* decisionVector, double* objectiveVector){

	int k= d_decisionNumber - d_objectiveNumber + 1;
	double g=0.0;
	double alpha=100.0;

	for(int i=d_decisionNumber-k;i<d_decisionNumber;i++)
		g+=(decisionVector[i]-0.5)*(decisionVector[i]-0.5);

	for(int i=0;i<d_objectiveNumber;i++)
		objectiveVector[i] = (1.0+g);

	for(int i=0;i<d_objectiveNumber;i++){
		for(int j=0;j<d_objectiveNumber-(i+1);j++)
			objectiveVector[i] *= cos(pow(decisionVector[j],alpha)*(M_PI/2));
			if(i != 0){
				int aux = d_objectiveNumber - (i+1);
				objectiveVector[i] *= sin(pow(decisionVector[aux],alpha)*(M_PI/2));
			}
	}
}
//evaluate a solution using the DTLZ5 problem
//params - the decision vector to be evaluated
//		 - the objective vector to hold the evaluation values
__device__ void Problem::calculateDTLZ5(double* decisionVector, double* objectiveVector){

	int k= d_decisionNumber - d_objectiveNumber + 1;
	double g=0.0;
	double theta[maxObjectiveNumber-1];

	for(int i=d_decisionNumber-k;i<d_decisionNumber;i++)
		g+=(decisionVector[i]-0.5)*(decisionVector[i]-0.5);

	double t = M_PI/(4.0*(1.0+g));

	theta[0]=decisionVector[0]*(M_PI/2.0);
	for(int i=1;i<(d_objectiveNumber-1);i++)
		theta[i] = t*(1.0+2.0*g*decisionVector[i]);

	for(int i=0;i<d_objectiveNumber;i++)
		objectiveVector[i] = (1.0+g);

	for(int i=0;i<d_objectiveNumber;i++){
		for(int j=0;j<d_objectiveNumber-(i+1);j++)
			objectiveVector[i] *= cos(theta[j]);
			if(i != 0){
				int aux = d_objectiveNumber - (i+1);
				objectiveVector[i] *= sin(theta[aux]) ;
			}
	}
}
//evaluate a solution using the DTLZ6 problem
//params - the decision vector to be evaluated
//		 - the objective vector to hold the evaluation values
__device__ void Problem::calculateDTLZ6(double* decisionVector, double* objectiveVector){

	int k= d_decisionNumber - d_objectiveNumber + 1;
	double g=0.0;
	double theta[maxObjectiveNumber-1];

	for(int i=d_decisionNumber-k;i<d_decisionNumber;i++)
		g+=pow(decisionVector[i],0.1);

	double t = M_PI/(4.0*(1.0+g));
	theta[0]=decisionVector[0]*(M_PI/2.0);
	for(int i=1;i<(d_objectiveNumber-1);i++)
		theta[i] = t*(1.0+2.0*g*decisionVector[i]);

	for(int i=0;i<d_objectiveNumber;i++)
		objectiveVector[i] = (1.0+g);

	for(int i=0;i<d_objectiveNumber;i++){
		for(int j=0;j<d_objectiveNumber-(i+1);j++)
			objectiveVector[i] *= cos(theta[j]);
			if(i != 0){
				int aux = d_objectiveNumber - (i+1);
				objectiveVector[i] *= sin(theta[aux]) ;
			}
	}
}
//evaluate a solution using the DTLZ7 problem
//params - the decision vector to be evaluated
//		 - the objective vector to hold the evaluation values
__device__ void Problem::calculateDTLZ7(double* decisionVector, double* objectiveVector){
	int k= d_decisionNumber - d_objectiveNumber + 1;
	double g=0.0;

	for(int i=d_decisionNumber-k;i<d_decisionNumber;i++)
		g+=decisionVector[i];

	g=1+(9.0*g)/k;

	for(int i=0;i<d_objectiveNumber-1;i++)
		objectiveVector[i] = decisionVector[i];

	double h=0.0;
	for(int i=0;i<d_objectiveNumber-1;i++)
		h+=(objectiveVector[i]/(1.0+g))*(1+sin(3.0*M_PI*objectiveVector[i]));

	h=d_objectiveNumber-h;

	objectiveVector[d_objectiveNumber-1]=(1+g)*h;
}

/**************************************************************************************  WFG FUNCTIONS ************************************************************************/

__device__ double correct_to_01(double a){
	double min = (double)0.0;
	double max = (double)1.0;
	double epsilon = (double)1e-7;

	double min_epsilon = min - epsilon;
	double max_epsilon = max + epsilon;

	if (( a <= min && a >= min_epsilon ) || (a >= min && a <= min_epsilon)) {
		return min;        
	} else if (( a >= max && a <= max_epsilon ) || (a <= max && a >= max_epsilon)) {
		return max;        
	} else {
		return a;        
	}
}
__device__ void normalize(double* decisionVector, double* result){
	for (int i = 0; i < d_decisionNumber; i++){
		double bound = (double)2.0 * (i + 1);
		result[i] = decisionVector[i] / bound;
		result[i]=correct_to_01(result[i]);
	}
}    
__device__ double s_linear(double y, double A){
	return correct_to_01(fabs(y - A) /(double)fabs(floor(A - y) + A));
}

__device__ double s_multi(double y, int A, int B, double C){                
	double tmp1, tmp2;
		
	tmp1 = ((double)4.0 * A + (double)2.0) * (double)M_PI * ((double)0.5 - fabs(y - C) / ((double)2.0 * ((double)floor(C - y) + C)));
	tmp2 = (double)4.0 * B * (double)pow(fabs(y - C) / ((double)2.0 * ((double)floor(C - y) + C)), (double)2.0);
		
	return correct_to_01(((double)1.0 + (double)cos(tmp1) + tmp2) / (B + (double)2.0));
}

__device__ double s_decept(double y, double A, double B, double C){        
	double tmp, tmp1, tmp2;
		
	tmp1 = (double)floor(y - A + B) * ((double)1.0 - C + (A - B)/B) / (A - B);
	tmp2 = (double)floor(A + B - y) * ((double)1.0 - C + ((double)1.0 - A - B) / B) / ((double)1.0 - A - B);
		
	tmp = fabs(y - A) - B;
		
	return correct_to_01((double)1 + tmp * (tmp1 + tmp2 + (double)1.0/B));
}

__device__ double b_flat(double y, double A, double B, double C){    
	double tmp1 = min((double)0, (double)floor(y - B))* A*(B-y)/B;
	double tmp2 = min((double)0, (double)floor(C - y))* (1 - A)*(y - C)/(1 - C);
		
	return correct_to_01(A + tmp1 - tmp2);
}
__device__ double b_poly(double y, double alpha){
	if (! ( y>=0 || y<=1 || alpha>0 || alpha != 1 ) )
		printf("ERROR ON WFG FUNCTION! (b_poly)");
	
	return correct_to_01((double)pow(y,alpha));
}
__device__ double b_param(double y, double u, double A, double B, double C){
	double result, v, exp;
		
	v = A - ((double)1.0 - (double)2.0 * u) * fabs((double)floor((double)0.5 - u) + A);
	exp = B + (C - B)*v;
	result = (double)pow(y,exp);
		
	return correct_to_01(result);                  
}
__device__ void subVector(double* z, int head, int tail, double* result){	
	for( int i = head; i <= tail; i++ ){
		result[i-head]=z[i];
	}
}

__device__ double r_sum(double* y, double* w, int length){
	double tmp1 = (double)0.0, tmp2 =(double) 0.0;
	for (int i = 0; i < length; i++){
		tmp1 += y[i]*w[i];
		tmp2 += w[i];
	}
		
	return correct_to_01(tmp1 / tmp2);
}
__device__ double r_nonsep(double* y, int A, int size){
	double tmp, denominator, numerator;
		
	tmp = (double)ceil(A/(double)2.0);        
	denominator = size * tmp * ((double)1.0 + (double)2.0*A - (double)2.0*tmp)/A;        
	numerator = (double)0.0;
	for (int j = 0; j < size; j++){
		numerator += y[j];
		for (int k = 0; k <= A-2; k++){
		numerator += fabs( y[j] - y[( j+k+1 ) % size]);
		}
	}
		
	return correct_to_01(numerator/denominator);
}

__device__ double convex(double* x, int m){
	double result = (double)1.0;
		
	for (int i = 1; i <= d_objectiveNumber - m; i++)
		result *= (1 - cos(x[i-1] * M_PI * 0.5));
					
	if (m != 1)
		result *= (1 - sin(x[d_objectiveNumber - m] * M_PI * 0.5));
		
	return result;
}
__device__ double concave(double* x, int m){
	double result = (double)1.0;
		
	for (int i = 1; i <= d_objectiveNumber - m; i++)
		result *= sin(x[i-1] * M_PI * 0.5);
		
	if (m != 1)
		result *= cos(x[d_objectiveNumber - m] * M_PI * 0.5);
			
	return result;
}
__device__ double linear(double* x, int m){
	double  result = (double)1.0;        

	for (int i = 1; i <= d_objectiveNumber - m; i++)
		result *= x[i-1];
		
	if (m != 1)        
		result *= (1 - x[d_objectiveNumber - m]);
				
	return result;
}

__device__ double mixed(double* x, int A, double alpha){
	double tmp;        
	tmp =(double) cos((double)2.0 * A * (double)M_PI * x[0] + (double)M_PI * (double)0.5);
	tmp /= (2.0 * (double) A * M_PI);
	return (double)pow(((double)1.0 - x[0] - tmp),alpha);
}

__device__ double disc(double* x, int A, double alpha, double beta){
	double tmp;
	tmp = (double)cos((double)A * pow(x[0], beta) * M_PI);
		
	return (double)1.0 - (double)pow(x[0],alpha) * (double)pow(tmp,2.0);
}

__device__ void Problem::calculate_x(double* t){
	double tmp[maxObjectiveNumber];
	double A[maxObjectiveNumber-1];
	
	if(!cudaStrncmp(problem.value, "wfg3", 4)){
		A[0] = 1;
		for (int i = 1; i < d_objectiveNumber-1; i++) 
			A[i] = 0;
	}else
		for (int i = 0; i < d_objectiveNumber-1; i++)
			A[i] = 1;
	
	
		
	for (int i = 0; i < d_objectiveNumber-1; i++){
		tmp[i] = max(t[d_objectiveNumber-1],A[i]) * (t[i]  - (double)0.5) + (double)0.5;
	}
		
	tmp[d_objectiveNumber-1] = t[d_objectiveNumber-1];
		
	memcpy(t, &tmp, sizeof(double)*d_objectiveNumber);
}

__device__ void WFG1_t1(double* z, int k){
	double tmp[maxDecisionNumber];
	
	memcpy(&tmp, z, sizeof(double)*d_decisionNumber);
		
	for (int i = k; i < d_decisionNumber; i++) {
		tmp[i]=s_linear(z[i],0.35);
	}
	
	memcpy(z, &tmp, sizeof(double)*d_decisionNumber);
}

__device__ void WFG4_t1(double* z){
	double tmp[maxDecisionNumber];
	
	for (int i = 0; i < d_decisionNumber; i++) {
		tmp[i] = s_multi(z[i],30,10,(double)0.35);
	}
	memcpy(z, &tmp, sizeof(double)*d_decisionNumber);
}

__device__ void WFG5_t1(double* z){
	double tmp[maxDecisionNumber];
	
	for (int i = 0; i < d_decisionNumber; i++) {
		tmp[i] = s_decept(z[i],(double)0.35,(double)0.001,(double)0.05);
	}
	memcpy(z, &tmp, sizeof(double)*d_decisionNumber);
}

__device__ void WFG7_t1(double* z, int k){
	double tmp[maxDecisionNumber];
	double w[maxDecisionNumber];
	double subZ[maxDecisionNumber];
	double subW[maxDecisionNumber];
	
	memcpy(&tmp, z, sizeof(double)*d_decisionNumber);

	for (int i = 0; i < d_decisionNumber; i++) {
		w[i] = 1;
	}

	for (int i = 0; i < k; i++){
		int head = i+1;
		int tail = d_decisionNumber-1;
		subVector(z,head,tail,subZ);
		subVector(w,head,tail,subW);
		double aux = r_sum(subZ,subW,(tail-head+1));

		tmp[i] = b_param(z[i],aux,(double)0.98/(double)49.98,(double)0.02,(double)50);
	}

	memcpy(z, &tmp, sizeof(double)*d_decisionNumber);
}

__device__ void WFG8_t1(double* z, int k){
	double tmp[maxDecisionNumber];
	double w[maxDecisionNumber];
	double subZ[maxDecisionNumber];
	double subW[maxDecisionNumber];
		
	for (int i = 0; i < d_decisionNumber; i++) {
		w[i] = (double)1.0;
	}

	memcpy(&tmp, z, sizeof(double)*d_decisionNumber);
		
	for (int i = k; i < d_decisionNumber; i++){
		int head = 0;
		int tail = i - 1;
		subVector(z,head,tail,subZ);
		subVector(w,head,tail,subW);            
		double aux = r_sum(subZ,subW,(tail-head+1));
			
		tmp[i] = b_param(z[i],aux,(double)0.98/(double)49.98,(double)0.02,50);
	}
		
	memcpy(z, &tmp, sizeof(double)*d_decisionNumber);
}

__device__ void WFG9_t1(double* z, int k){
	double tmp[maxDecisionNumber];
	double w[maxDecisionNumber];
	double subZ[maxDecisionNumber];
	double subW[maxDecisionNumber];
		
	for (int i = 0; i < d_decisionNumber; i++) {
		w[i] = (double)1.0;
	}

	memcpy(&tmp, z, sizeof(double)*d_decisionNumber);
		
	for (int i = 0; i < d_decisionNumber-1; i++){
      int head = i+1;
      int tail = d_decisionNumber-1;
      subVector(z,head,tail, subZ);
      subVector(w,head,tail,subW);
      double aux = r_sum(subZ,subW,(tail-head+1));
      tmp[i] = b_param(z[i],aux,(double)0.98/(double)49.98,(double)0.02,(double)50);
    }
        
    tmp[d_decisionNumber-1] = z[d_decisionNumber-1];
		
	memcpy(z, &tmp, sizeof(double)*d_decisionNumber);
}

__device__ void WFG1_t2(double* z, int k){
	double tmp[maxDecisionNumber];

	memcpy(&tmp, z, sizeof(double)*d_decisionNumber);
		
	for (int i = k; i < d_decisionNumber; i++) {
		tmp[i]=b_flat(z[i],(double)0.8,(double)0.75,(double)0.85);
	}
		
	memcpy(z, &tmp, sizeof(double)*d_decisionNumber);
}

__device__ void WFG2_t2(double* z, int k){
	double tmp[maxDecisionNumber];
	double subZ[maxDecisionNumber];
	memcpy(&tmp, z, sizeof(double)*d_decisionNumber);
	
	int l = d_decisionNumber - k;
		
	for (int i = k+1; i <= k + l/2; i++){
		int head = k + 2*(i - k) - 1;
		int tail = k + 2*(i - k);              
		subVector(z,head-1,tail-1,subZ);
		
		tmp[i-1] = r_nonsep(subZ,2,(tail-head+1) );
	}
	memcpy(z, &tmp, sizeof(double)*d_decisionNumber);
}

__device__ void WFG4_t2(double* z, int k){
	double tmp[maxObjectiveNumber];
	double w[maxDecisionNumber];
	double subZ[maxDecisionNumber];
	double subW[maxDecisionNumber];

	for (int i = 0; i < d_decisionNumber; i++) {
		w[i] = (double)1.0;
	}
			
	for (int i = 1; i <= d_objectiveNumber-1; i++) {
		int head = (i - 1)*k/(d_objectiveNumber-1) + 1;
		int tail = i * k / (d_objectiveNumber - 1);
		subVector(z,head-1,tail-1,subZ);
		subVector(w,head-1,tail-1,subW);
		
		tmp[i-1] = r_sum(subZ,subW,(tail-head+1));
	}
		
	int head = k + 1;
	int tail = d_decisionNumber;
		
	subVector(z,head-1,tail-1,subZ);
	subVector(w,head-1,tail-1,subW);
	tmp[d_objectiveNumber-1] = r_sum(subZ,subW,(tail-head+1));
		
	memcpy(z, &tmp, sizeof(double)*d_objectiveNumber);
}

__device__ void WFG6_t2(double* z, int k){
	double tmp[maxObjectiveNumber];
	double subZ[maxDecisionNumber];
		
	for (int i = 1; i <= d_objectiveNumber-1; i++){
		int head = (i - 1)*k/(d_objectiveNumber-1) + 1;
		int tail = i * k / (d_objectiveNumber - 1);
		subVector(z,head-1,tail-1, subZ);            
			
		tmp[i-1] = r_nonsep(subZ,k/(d_objectiveNumber-1),(tail-head+1));            
	}
		
	int head = k + 1;
	int tail = d_decisionNumber;
	int l = d_decisionNumber - k;
			
	subVector(z,head-1,tail-1, subZ);              
	tmp[d_objectiveNumber-1] = r_nonsep(subZ,l,(tail-head+1));
				
	memcpy(z, &tmp, sizeof(double)*d_objectiveNumber);
}

__device__ void WFG9_t2(double* z, int k){
	double tmp[maxDecisionNumber];
		
	for (int i = 0; i < k; i++) {
		tmp[i] = s_decept(z[i],(double)0.35,(double)0.001,(double)0.05);
	}
		
	for (int i = k; i < d_decisionNumber; i++) {
		tmp[i] = s_multi(z[i],30,95,(double)0.35);
	}        

	memcpy(z, &tmp, sizeof(double)*d_decisionNumber);
}

__device__ void WFG1_t3(double* z){
	double tmp[maxDecisionNumber];
		
	for (int i = 0; i < d_decisionNumber; i++) {
		tmp[i]=b_poly(z[i],(double)0.02);
	}
		
	memcpy(z, &tmp, sizeof(double)*d_decisionNumber);
}

__device__ void WFG2_t3(double* z, int k){
	double tmp[maxObjectiveNumber];
	double w[maxDecisionNumber];
	
	double subZ[maxDecisionNumber];
	double subW[maxDecisionNumber];

	for (int i = 0; i < d_decisionNumber; i++) {
		w[i] = (double)1.0;
	}
		
	for (int i = 1; i <= d_objectiveNumber-1; i++){
		int head = (i - 1)*k/(d_objectiveNumber-1) + 1;
		int tail = i * k / (d_objectiveNumber - 1);
		subVector(z,head-1,tail-1,subZ);
		subVector(w,head-1,tail-1,subW);
		
		tmp[i-1] = r_sum(subZ,subW,(tail-head+1));
	}
		
	int l = d_decisionNumber - k;
	int head = k + 1;
	int tail = k + l / 2;
	subVector(z,head-1,tail-1, subZ);
	subVector(w,head-1,tail-1, subW);

	tmp[d_objectiveNumber-1] = r_sum(subZ,subW,(tail-head+1));
				
	memcpy(z, &tmp, sizeof(double)*d_objectiveNumber);
}

__device__ void WFG7_t3(double* z, int k){
	double tmp[maxObjectiveNumber];
	double w[maxDecisionNumber];
	double subZ[maxDecisionNumber];
	double subW[maxDecisionNumber];
		
	for (int i = 0; i < d_decisionNumber; i++) {
		w[i] = (double)1.0;
	}
		
	for (int i = 1; i <= d_objectiveNumber-1; i++){
		int head = (i - 1)*k/(d_objectiveNumber-1) + 1;
		int tail = i * k / (d_objectiveNumber - 1);
		subVector(z,head-1,tail-1, subZ);
		subVector(w,head-1,tail-1, subZ);
			
		tmp[i-1] = r_sum(subZ,subW,(tail-head+1));
	}
		
	int head = k + 1;
	int tail = d_decisionNumber;
	subVector(z,head-1,tail-1, subZ);
	subVector(w,head-1,tail-1, subW);
	tmp[d_objectiveNumber-1] = r_sum(subZ,subW,(tail-head+1));

	memcpy(z, &tmp, sizeof(double)*d_objectiveNumber);
}

__device__ void WFG1_t4(double* z, int k){
	double tmp[maxObjectiveNumber];	
	double w[maxDecisionNumber];
	double subZ[maxDecisionNumber];
	double subW[maxDecisionNumber];
				
	for (int i = 0; i < d_decisionNumber; i++) {
		w[i] = (double)2.0 * (i + 1);
	}
		
	for (int i = 1; i <= d_objectiveNumber-1; i++){
		int head = (i - 1)*k/(d_objectiveNumber-1) + 1;
		int tail = i * k / (d_objectiveNumber - 1);
		
		subVector(z,head-1,tail-1,subZ);
		subVector(w,head-1,tail-1,subW);		
		
		tmp[i-1]=r_sum(subZ,subW,(tail-head+1));
	}
		
	int head = k + 1 - 1;
	int tail = d_decisionNumber - 1;    
	
	subVector(z,head,tail,subZ);
	subVector(w,head,tail,subW);
	
	tmp[d_objectiveNumber-1]=r_sum(subZ,subW,(tail-head+1));
	
	memcpy(z, &tmp, sizeof(double)*d_objectiveNumber);
}
// __device__ bool vectorIn01(double* x){
// 	for( int i = 0; i < d_decisionNumber; i++ ){
// 		if( x[i] < 0.0 || x[i] > 1.0 ){
// 			return false;
// 		}
// 	}
// 
//   return true;
// }

__device__ void Problem::calculateWFG1(double* decisionVector, double* objectiveVector){
	double y[maxDecisionNumber];
	int k=4;
	if(d_objectiveNumber > 2)
		k=2*(d_objectiveNumber-1);

	normalize(decisionVector, y);	
	WFG1_t1(y,k);
	WFG1_t2(y,k);	
	WFG1_t3(y);
	WFG1_t4(y,k);
	
	calculate_x(y);
	
	for (int m = 1; m <= d_objectiveNumber - 1 ; m++) {
		objectiveVector[m-1] = y[d_objectiveNumber-1] + d_superiorPositionLimit[m-1] * convex(y,m);
	}
	objectiveVector[d_objectiveNumber-1] = y[d_objectiveNumber-1] + d_superiorPositionLimit[d_objectiveNumber-1] * mixed(y,5,(double)1.0);
}

__device__ void Problem::calculateWFG2(double* decisionVector, double* objectiveVector){
	double y[maxDecisionNumber];
	int k=4;
	if(d_objectiveNumber > 2)
		k=2*(d_objectiveNumber-1);
	
	normalize(decisionVector, y);
	WFG1_t1(y,k); // = wfg2_t1
	WFG2_t2(y,k);
	WFG2_t3(y,k);
		
	for (int m = 1; m <= d_objectiveNumber - 1 ; m++) {
		objectiveVector[m-1] = y[d_objectiveNumber-1] + d_superiorPositionLimit[m-1] * convex(y,m);
	}        
	objectiveVector[d_objectiveNumber-1] = y[d_objectiveNumber-1] + d_superiorPositionLimit[d_objectiveNumber-1] * disc(y,5,(double)1.0,(double)1.0);
}

__device__ void Problem::calculateWFG3(double* decisionVector, double* objectiveVector){
	double y[maxDecisionNumber];
	int k=4;
	if(d_objectiveNumber > 2)
		k=2*(d_objectiveNumber-1);	
	
	normalize(decisionVector, y);
	WFG1_t1(y,k); //=wfg3_t1
	WFG2_t2(y,k); //=wfg3_t2
	WFG2_t3(y,k); //=wfg3_t3

	calculate_x(y);        
	for (int m = 1; m <= d_objectiveNumber ; m++) {
		objectiveVector [m-1] = y[d_objectiveNumber-1] + d_superiorPositionLimit[m-1] * linear(y,m);
	}
}
__device__ void Problem::calculateWFG4(double* decisionVector, double* objectiveVector){
	double y[maxDecisionNumber];
	int k=4;
	if(d_objectiveNumber > 2)
		k=2*(d_objectiveNumber-1);
	
	normalize(decisionVector, y);
	WFG4_t1(y);
	WFG4_t2(y,k);

	calculate_x(y);
	for (int m = 1; m <= d_objectiveNumber ; m++) {
		objectiveVector [m-1] = y[d_objectiveNumber-1] + d_superiorPositionLimit[m-1] * concave(y,m);
	}
}

__device__ void Problem::calculateWFG5(double* decisionVector, double* objectiveVector){
	double y[maxDecisionNumber];
	int k=4;
	if(d_objectiveNumber > 2)
		k=2*(d_objectiveNumber-1);
	
	normalize(decisionVector, y);
	WFG5_t1(y);
	WFG4_t2(y,k); //=wfg5_t2
		
	calculate_x(y);
	for (int m = 1; m <= d_objectiveNumber ; m++) {
		objectiveVector [m-1] = y[d_objectiveNumber-1] + d_superiorPositionLimit[m-1] * concave(y,m);
	}
}
__device__ void Problem::calculateWFG6(double* decisionVector, double* objectiveVector){
	double y[maxDecisionNumber];
	int k=4;
	if(d_objectiveNumber > 2)
		k=2*(d_objectiveNumber-1);
	
	normalize(decisionVector, y);
	WFG1_t1(y,k); //=wfg6_t1
	WFG6_t2(y,k);

	calculate_x(y);
	for (int m = 1; m <= d_objectiveNumber ; m++) {
		objectiveVector[m-1] = y[d_objectiveNumber-1] + d_superiorPositionLimit[m-1] * concave(y,m);
	}
}

__device__ void Problem::calculateWFG7(double* decisionVector, double* objectiveVector){
	double y[maxDecisionNumber];
	int k=4;
	if(d_objectiveNumber > 2)
		k=2*(d_objectiveNumber-1);
	
	normalize(decisionVector, y);
	WFG7_t1(y,k);
	WFG1_t1(y,k); //=wfg7_t2
	WFG7_t3(y,k);
		
	calculate_x(y);
	for (int m = 1; m <= d_objectiveNumber ; m++) {
		objectiveVector[m-1] = y[d_objectiveNumber-1] + d_superiorPositionLimit[m-1] * concave(y,m);
	}
}
__device__ void Problem::calculateWFG8(double* decisionVector, double* objectiveVector){
	double y[maxDecisionNumber];
	int k=4;
	if(d_objectiveNumber > 2)
		k=2*(d_objectiveNumber-1);
	
	normalize(decisionVector, y);
	WFG8_t1(y,k);
	WFG1_t1(y,k); //=wfg8_t2
	WFG4_t2(y,k); //=wfg8_t3
		
	calculate_x(y);        
	for (int m = 1; m <= d_objectiveNumber ; m++) {
		objectiveVector [m-1] = y[d_objectiveNumber-1] + d_superiorPositionLimit[m-1] * concave(y,m);                
	}        
}

__device__ void Problem::calculateWFG9(double* decisionVector, double* objectiveVector){
	double y[maxDecisionNumber];
	int k=4;
	if(d_objectiveNumber > 2)
		k=2*(d_objectiveNumber-1);
	
	WFG9_t1(y,k);
	WFG9_t2(y,k);
	WFG6_t2(y,k); //=wfg9_t3
			
	calculate_x(y);        
	for (int m = 1; m <= d_objectiveNumber ; m++) {
		objectiveVector [m-1] = y[d_objectiveNumber-1] + d_superiorPositionLimit[m-1] * concave(y,m);                
	}        
}