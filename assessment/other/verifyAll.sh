#entradas="novo/novo"\("0,0"\)" smpso/smpso"\("2,2"\)" smpso/smpso"\("2,3"\)" smpso/smpso"\("2,4"\)" smpso/smpso"\("3,2"\)" smpso/smpso"\("3,3"\)" smpso/smpso"\("3,4"\)" smpso/smpso"\("4,2"\)" smpso/smpso"\("4,3"\)" smpso/smpso"\("4,4"\)""

entradas=$(cat results/titles.txt | tail -1)

comando=""
EFS="
"
for nomeSaida in $entradas; do
	echo "-------------$nomeSaida-------------"
	for problem in dtlz7 wfg1 wfg4 wfg6; do
		for objectives in 3 5 8 10 15 20; do
			comando=$comando"results/$nomeSaida""$problem-$objectives"_"fronts.txt "
		
			java -Xmx128m -cp assessment/other/ verifyFronts $comando
			
			unset comando;
		done
	done
done

