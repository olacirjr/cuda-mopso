#nomeSaida="smpso"
#nomeSaida="novo"
#leader=4
#archiver=4

# for leader in 2 3 4; do
# 	for archiver in 2 3 4; do
# 		mkdir "results/$nomeSaida"
# 		for problem in 1 2 3 4 5 6 7; do
# 			for objectives in 2 3 5 10 15 20; do
# 				paramFile="parameters/$nomeSaida($leader,$archiver)$problem-$objectives.txt"
# 				file="results/$nomeSaida/$nomeSaida($leader,$archiver)DTLZ$problem-$objectives"
# 				echo $file
# 
# 				echo "outName=$file" > $paramFile
# 				echo "problem=$problem" >> $paramFile
# 				echo "leader=$leader" >> $paramFile
# 				echo "archiver=$archiver" >> $paramFile
# 				echo "objectiveNumber=$objectives" >>$paramFile
# 				echo "population=100" >> $paramFile
# 				echo "repository=100" >> $paramFile
# 				echo "swarms=1" >> $paramFile
# 				echo "iterations=100" >> $paramFile
# 				echo "runs=30" >> $paramFile
# 				echo "IMulti=false" >> $paramFile
# 				echo "partIterations=5" >> $paramFile
# 				echo "novo=false" >> $paramFile
# 
# # 				if [ $problem = "7" ] ; then
# # 					echo sete
# # 				else
# # 					echo $problem
# # 				fi
# 				
# 				./cuda_mopso.out $paramFile
# 				#metrics/metrics.sh $file $file $problem $objectives &
# 			done
# 		done
# 	done
# done
# 
# nomeSaida="smpso"
# leader=2
# archiver=2
# mkdir "results/$nomeSaida"
# for problem in 1 2 3 4 5 6 7; do
# 	#for objectives in 2 3 5 10 15 20; do
# 	for objectives in 2 3 5 8 9; do
# 		paramFile="parameters/$nomeSaida($leader,$archiver)$problem-$objectives.txt"
# 		file="results/$nomeSaida/$nomeSaida($leader,$archiver)DTLZ$problem-$objectives"
# 		echo $file
# 
# 		echo "outName=$file" > $paramFile
# 		echo "problem=$problem" >> $paramFile
# 		echo "leader=$leader" >> $paramFile
# 		echo "archiver=$archiver" >> $paramFile
# 		echo "objectiveNumber=$objectives" >>$paramFile
# 		case "$objectives" in
# 			*2* ) 	echo "population=600" >> $paramFile
# 					echo "repository=600" >> $paramFile
# 					echo "iterations=499" >> $paramFile
# 			;;
# 			*3* ) 	echo "population=1000" >> $paramFile
# 					echo "repository=1000" >> $paramFile
# 					echo "iterations=299" >> $paramFile
# 			;;
# 			*5* ) 	echo "population=1200" >> $paramFile
# 					echo "repository=1200" >> $paramFile
# 					echo "iterations=249" >> $paramFile
# 			;;
# 			*8* ) 	echo "population=1200" >> $paramFile
# 					echo "repository=1200" >> $paramFile
# 					echo "iterations=249" >> $paramFile
# 			;;
# 			*9* ) 	echo "population=1200" >> $paramFile
# 					echo "repository=1200" >> $paramFile
# 					echo "iterations=249" >> $paramFile
# 			;;
# 			* ) echo "Error, undetermined objective number!"; exit ;;
# 		esac
# 		#echo "population=100" >> $paramFile
# 		#echo "repository=100" >> $paramFile
# 		#echo "iterations=100" >> $paramFile
# 		echo "swarms=1" >> $paramFile
# 		echo "runs=30" >> $paramFile
# 		echo "IMulti=false" >> $paramFile
# 		echo "partIterations=5" >> $paramFile
# 		echo "novo=false" >> $paramFile
# 
# 		./cuda_mopso.out $paramFile
# 	done
# 	#assessment/metrics/juntar_tudo_gd.sh $problem &
# 	#assessment/metrics/juntar_tudo_igd.sh $problem &
# 	#assessment/metrics/juntar_tudo_r2.sh $problem &
# done

nomeSaida="cmulti"
leader=0
archiver=0
mkdir "results/$nomeSaida"
#problem=$1
#for problem in dtlz1 dtlz2 dtlz3 dtlz4 dtlz5 dtlz6 dtlz7 wfg1 wfg2 wfg3 wfg4 wfg5 wfg6 wfg7 wfg8 wfg9; do
for problem in $1; do
	for objectives in 3 5 8 10 15 20; do
		for sw in 50; do
			#for objectives in 2 3 5 8 9; do
			paramFile="parameters/$nomeSaida($sw)$problem-$objectives.txt"
			file="results/$nomeSaida/$nomeSaida($sw)$problem-$objectives"
			echo $file

			echo "outName=$file" > $paramFile
			echo "problem=$problem" >> $paramFile
			echo "leader=$leader" >> $paramFile
			echo "archiver=$archiver" >> $paramFile
			echo "objectiveNumber=$objectives" >>$paramFile
	# 		case "$objectives" in
	# 			*2* ) 	echo "population=600" >> $paramFile
	# 					echo "repository=600" >> $paramFile
	# 					echo "iterations=499" >> $paramFile
	# 			;;
	# 			*3* ) 	echo "population=1000" >> $paramFile
	# 					echo "repository=1000" >> $paramFile
	# 					echo "iterations=299" >> $paramFile
	# 			;;
	# 			*5* ) 	echo "population=1200" >> $paramFile
	# 					echo "repository=1200" >> $paramFile
	# 					echo "iterations=249" >> $paramFile
	# 			;;
	# 			*8* ) 	echo "population=1200" >> $paramFile
	# 					echo "repository=1200" >> $paramFile
	# 					echo "iterations=249" >> $paramFile
	# 			;;
	# 			*9* ) 	echo "population=1200" >> $paramFile
	# 					echo "repository=1200" >> $paramFile
	# 					echo "iterations=249" >> $paramFile
	# 			;;
	# 			* ) echo "Error, undetermined objective number!"; exit ;;
	# 		esac
			echo "population=100" >> $paramFile
			echo "repository=200" >> $paramFile
			echo "iterations=200" >> $paramFile
			echo "swarms=$sw" >> $paramFile
			echo "runs=30" >> $paramFile
			echo "algorithm=cmulti" >> $paramFile
			echo "partIterations=5" >> $paramFile
			echo "archSubSwarms=cd" >> $paramFile

			./cuda_mopso.out $paramFile $2
		done
	done
	#assessment/metrics/juntar_tudo_gd.sh $problem &
	#assessment/metrics/juntar_tudo_igd.sh $problem &
	#assessment/metrics/juntar_tudo_r2.sh $problem &
done
echo "PRONTO!"


