/*
subregions assignment test
*/

#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <limits>
#include <math.h>  

using namespace std;

vector< vector<double> > load_weight_vectors(string file, int n, int m);
vector<int> subregion_assignment(vector< vector<double> > weight_vectors, vector< vector<double> > solutions);

int main() {
	int n = 3, m = 2; 
	
	vector< vector<double> > weight_vectors = load_weight_vectors("2.dat", n, m);
	
	vector< vector<double> > solutions(4, vector<double>(m));
	solutions[0][0] = 1.0;
	solutions[0][1] = 1.0;
	solutions[1][0] = 0.5;
	solutions[1][1] = 1.5;
	solutions[2][0] = 0.0;
	solutions[2][1] = 2.0;
	solutions[3][0] = 1.0;
	solutions[3][1] = 0.0;

	
	vector<int> subregions = subregion_assignment(weight_vectors, solutions);	
	for (vector<int>::iterator i = subregions.begin(); i != subregions.end(); ++i) {
		printf("%d\t", *i);
	}
	printf("\n");
	return 0;
}

vector< vector<double> > load_weight_vectors(string file, int n, int m) {
	vector< vector<double> > weight_vectors(n, vector<double>(m));
	FILE *stream = fopen(file.c_str(), "r");
	if( stream == NULL ){
		printf("\nFail to open Weight Vectors File!!\n");
		exit(1);
	}
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < m; ++j)
			fscanf(stream, "%lf", &weight_vectors[i][j]);
	
	fclose(stream);
	return weight_vectors;
}

vector<int> subregion_assignment(vector< vector<double> > weight_vectors, vector< vector<double> > solutions ){
	vector <int> subregions (solutions.size());
	vector <double> angle (solutions.size(), numeric_limits<double>::max());
	int r, i, j;
	double dot, lenx, lenw, x, w, a;

	// iterate over the solutions
	j = 0;
	for(std::vector< vector<double> >::iterator s = solutions.begin(); s != solutions.end(); ++s, ++j) {
    	// iterate over the weight vectors and subregions
    	r = 0;
		for(std::vector< vector<double> >::iterator v = weight_vectors.begin(); v != weight_vectors.end(); ++v, ++r) {
    		// iterate over the objectives
    		dot = lenw = lenx = 0.0;
    		i = 0;
    		for(std::vector<double>::iterator o = (*v).begin(); o != (*v).end(); ++o, ++i) {
    			x = (*s)[i];
    			w = (*o);
    			dot += (x*w);
    			lenx += (x*x);
    			lenw += (w*w);
    		}
    		a = acos(dot/(sqrt(lenx * lenw)));
    		if (a <= angle[j]){
    			subregions[j] = r;
    			angle[j] = a;
    		}
    	}	
	}

	return subregions;

}