#!/bin/bash

m=3
h1=2
h2=1
t=0.5

g++ -o wgen wgen.cpp

if [ "$?" = "0" ]; then
	#1 layer generation
	./wgen -h $h1 -m $m -t 1.0 > first_layer.dat

	#2 layer generation
	./wgen -h $h2 -m $m -t $t > second_layer.dat

	cp first_layer.dat $m.dat
	cat second_layer.dat >> $m.dat

fi

