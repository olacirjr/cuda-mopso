/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
wgen.cpp
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <iostream>
#include <map>
#include <stdlib.h>
#include <cmath>
#include <vector>
#include <numeric>

using namespace std;

void wgen(int m, int h, double d, double t);
void print(vector<int> v, double d, double t, int m);
vector<int> update (vector<int> v, int h);

/* *
* @param 
* -h  <int> default 3   //number of divisions
* -m  <int> default 2   //number of objectives
* -t  <int> default 1.0 //shrinkage factor
* */

int main(int argc, char *argv[]) {

	int h=3, m = 2;
	double t = 1.0, d;

	/* convert args[] to map<arg, value> */
	map<string, string> arguments;
	for (int i=1; i<argc; i+=2)
		arguments[argv[i]] = argv[i+1];

	if (arguments.find("-h") != arguments.end())
		h = atoi(arguments["-h"].c_str());
	if (arguments.find("-m") != arguments.end())
		m = atoi(arguments["-m"].c_str());
	if (arguments.find("-t") != arguments.end())
		t = atof(arguments["-t"].c_str());

	d = 1.0 / (double) h;
	wgen(m, h, d, t);

	return 0;
}

void wgen(int m, int h, double d, double t){
	vector<int> count;
	for (int i = 0; i < m; ++i){
		count.push_back(0);
	}
	for (int j = 1; j < pow(h+1, m); ++j) {
		count = update(count, h);
		if (h == accumulate(count.begin(),count.end(),0))
			print(count, d, t, m);
	}
}

void print(vector<int> v, double d, double t, int m){
	for (vector<int>::iterator i = v.begin(); i != v.end(); ++i) {
		cout << (((1.0 - t)/ ((double) m)) + ( t * ((double) *i)  * d)) << "\t";
	}
	cout << endl;
}

vector<int> update (vector<int> v, int h) {
	for (vector<int>::iterator i = v.begin(); i != v.end(); i = ((*i != 0) ? v.end() : i+1))
		(++(*i))%=(h+1);
	return v;
}