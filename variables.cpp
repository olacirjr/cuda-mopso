struct str { char value[10]; }; //using type str due to difficulties in passing arrays of chars to cuda

const int maxObjectiveNumber=20;
const int maxSwarmSize=200;//number of particles in the swarm
const int maxSwarmNumber=50; //the number of swarms used in the search
const int maxRepositorySize=200;//swarmMaxSize*maxSwarmNumber; //number maximum of particles stored in the repository
const int maxDecisionNumber=20+(2*(maxObjectiveNumber-1)); //to acomodate the WFG problems

//const int maxDecisionNumber=maxObjectiveNumber+maxK-1; //the total number of decision variables of the problem, calculated according to k and the objective number

//const int k=10; // defines the number of decision variables (decisionNumber=objectiveNumber+k-1)

double mutationIndice=0.15; //percentage of the particles to be influenced by the turbulence operator
__device__ double d_superiorPositionLimit[maxDecisionNumber];
__device__ double d_inferiorPositionLimit[maxDecisionNumber];
double superiorPositionLimit[maxDecisionNumber];
double inferiorPositionLimit[maxDecisionNumber];


const double MAX_PHI=1;
const double MAX_OMEGA=0.8;
const double VELOCITY_REDUCTION=0.001;
const double MAX_MUT=0.5;
const double INERTIA=0.1;
// const double superiorVelocityLimit=5; //Andre
// const double inferiorVelocityLimit=-5; //Andre
__device__ double deltaMax[maxDecisionNumber]; //JMetal
__device__ double deltaMin[maxDecisionNumber]; //JMetal

const int maxSigmaSize=190; //190 //for 20 objectives the sigma vector size is 190

__device__ int d_objectiveNumber, d_decisionNumber, sigmaSize;
int objectiveNumber, decisionNumber;

str problem; //DTLZ(1-7) or WFG(1-9)
char outputFileName[100]; //name of the output file
int swarmSize; //population
int repositorySize; //maximum number of solutions that the repository can hold
int swarmNumber; //the number of swarms used in the search
int maxIterations; //number of iterations of of the search
int numExec; //number of independent runs
str archiver; //random (rnd), crowding distance (cd), ideal, mga
str leader; //random (rnd), crowding distance (cd), NWSum, Sigma
str algorithm; //which algorithm is to be executed


//nBlocks - number of times in paralell the code will run
int blockSize, nBlocks;
char _s[100]; //name of file to store the optimal solutions
char _f[100]; //name of file to store the optimal front
char _p[100]; //name of file to store the probabilities at each iteration
curandState* devStates; //devstates of the curand generator