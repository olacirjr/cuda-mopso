#include <cuda.h>
#include <curand.h>
#include <curand_kernel.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <values.h>
#include <string.h>
#include <time.h>

#include "variables.cpp"
#include "Problem.cpp"
#include "Solution.cpp"
#include "Particle.cpp"
#include "Repository.cpp"
#include "Swarm.cpp"
#include "util.cpp"
#include "EDA/eda.cpp"

RepositoryFinal repFinal;
Swarm h_swarm[maxSwarmNumber]; //Swarms on host
__device__ Swarm d_swarm[maxSwarmNumber]; //Swarms on device (GPU)
//I-Multi parameters
int numParticoes; //number of partitioning iterations (I-Multi)
char archSubSwarms[50];
double rangeInicial=0.5; //(I-Multi)
double rangeFinal=0.1; //(I-Multi)
int subSwarmPopulation;//25;
double incremento=fabs(rangeInicial-rangeFinal)/numParticoes;//(I-Multi)
double range=rangeInicial;//(I-Multi)
bool h_diversityPhase=false; //diversity phase of the IMulti algorithm
__device__ bool d_diversityPhase; //diversity phase of the IMulti algorithm
Swarm h_initialSwarm; //initial swarm on host
__device__ Swarm d_initialSwarm; //initial swarm on device
// end of I-Multi parameters

//H-MOPSO, SMAB parameters
Repository repPrevious;
const int combinacoes=9;
double probabilidades[combinacoes];
double reference[3100][maxObjectiveNumber];
char arquivo[50];
int tamRef=-1;
int usada=-1;
//end of H-MOPSO, SMAB parameters

//prototipation of the functions to be used
void IMultiOperations(int iteration, int run);
void hmopsoBefore();
void hmopsoAfter();
__global__ void cudaInitializeParticles(int swarmSize, int swarmNumber, curandState* devStates, int run, str problem, int objectiveNumber, int decisionNumber, bool diversityPhase, str leader);
void initializeRepository();
__global__ void chooseGlobalLeaders(int swarmSize, int swarmNumber, curandState* devStates);
__global__ void calculateVelocity(int swarmSize, int swarmNumber, curandState* devStates);
__global__ void updatePosition(int swarmSize,  int swarmNumber);
__global__ void turbulence(int swarmSize, int swarmNumber, curandState* devStates, int mutationIndice);
__global__ void evaluation(int swarmSize, int swarmNumber, double range, str leader, str algorithm);
void updateRepository();
__global__ void updateParticlesMemory(int swarmSize, int swarmNumber, curandState* devStates);
void mostrador(int iteration);
void readParameters(int argc, char *inputFile);

//*********************************************************************************************************************************************************************//
int main(int argc, char* argv[]){
	if(argc > 2 ){
		int count;
		cudaGetDeviceCount(&count);
		if(count > atoi(argv[2]) ){
			cudaSetDevice(atoi(argv[2]));
			printf("Using video board: %s\n",argv[2]);
		}
		else{
			printf("Video board unavailable\n");
			exit(1);
		}
	}	
	printf("Allocating memory...\n");
	readParameters(argc, argv[1]);
	cudaMalloc ( &devStates, maxSwarmSize*maxSwarmNumber*sizeof( curandState ) );
	printf("done!\n");
	
	subSwarmPopulation=(750/swarmNumber);
	sprintf(_s, "%s_solutions.txt",outputFileName);
	sprintf(_f, "%s_fronts.txt",outputFileName);
	clearFile(_s); //clear the solutions file, so it previous content is deleted
	clearFile(_f); //clear the front file, so it previous content is deleted
	
	if(!strncmp(algorithm.value, "hmopso", 6)){
		sprintf(_p, "%s_probabilities.txt",outputFileName);
		clearFile(_p); //clear the front file, so it previous content is deleted
		sprintf(arquivo, "ref/%d-obj.txt",objectiveNumber);
		tamRef=lerArquivos(reference, arquivo);
	}

	for(int run=0;run<numExec;run++){ //independent runs loop
		printf("----------------Independent run # %d-------------- (%s - %d OBJ) \n", run, problem.value, objectiveNumber);
		readParameters(argc, argv[1]); //read all the parameters again, case some parameter had changed during the run
		
		if( ( !strncmp(algorithm.value, "imulti", 5) || !strncmp(algorithm.value, "cmulti", 5) ) ){
			range=rangeInicial;
			h_initialSwarm.repository.clear();
			h_diversityPhase=true;
			//archiver=3;
			sprintf(archiver.value, "mga");
			sprintf(leader.value, "cd");
		}
		if(!strncmp(algorithm.value, "hmopso", 6)){
			for(int i=0;i<combinacoes;i++)
				probabilidades[i]=1.0/combinacoes;
			sprintf(archiver.value, "cd"); //initialize with smpso, only change to other when the repository is full
			sprintf(leader.value, "cd");
		}
			
		cudaInitializeParticles <<< nBlocks, blockSize >>> (swarmSize, swarmNumber, devStates, run, problem, objectiveNumber, decisionNumber, h_diversityPhase, leader);//initialize the particles in paralell
		initializeRepository(); //the repository operations are done in the host

		for(int iteration=0;iteration<maxIterations;iteration++){ //main loop
			
			hmopsoBefore();
			
			if(h_diversityPhase || strncmp(algorithm.value, "cmulti", 5)){ //if diversity phase or not cmulti
				chooseGlobalLeaders <<< nBlocks, blockSize >>> (swarmSize, swarmNumber, devStates);
				
				calculateVelocity  <<< nBlocks, blockSize >>> (swarmSize, swarmNumber, devStates);
				
				updatePosition  <<< nBlocks, blockSize >>> (swarmSize, swarmNumber);
				
				turbulence <<< nBlocks, blockSize >>> (swarmSize, swarmNumber, devStates, mutationIndice);
			}
			
			IMultiOperations(iteration, run); //if imulti or cmulti it overrites the actions done before if it is the case
			
			evaluation <<< nBlocks, blockSize >>> (swarmSize, swarmNumber, range, leader, algorithm);

			updateParticlesMemory <<< nBlocks, blockSize >>> (swarmSize, swarmNumber, devStates);			

			updateRepository();//the repository operations are done in the host
			
			hmopsoAfter();
			
// 			printf("\n\n\n particle: ");
// 			printVector(h_swarm[0].particles[0].solution.objectiveVector, objectiveNumber);
// 			printf(" -> ");
// 			printVector(h_swarm[0].particles[0].solution.decisionVector, decisionNumber);
// 			printf(" (vel: ");
// 			printVector(h_swarm[0].particles[0].velocity, decisionNumber);
// 			printf(")\n p_leader: ");
// 			printVector(h_swarm[0].particles[0].localBest.objectiveVector, objectiveNumber);
// 			printf(" -> ");
// 			printVector(h_swarm[0].particles[0].localBest.decisionVector, decisionNumber);
// 			printf("\n g_leader: ");
// 			printVector(h_swarm[0].particles[0].globalBest.objectiveVector, objectiveNumber);
// 			printf(" -> ");
// 			printVector(h_swarm[0].particles[0].globalBest.decisionVector, decisionNumber);
// 			
// 			for(int s=0;s<swarmNumber;s++){
// 				for(int p=0;p<h_swarm[s].repository.getActualSize();p++){
// 					printf("\n rep_%d: ",p);
// 					printVector(h_swarm[s].repository.getSolution(p).objectiveVector, objectiveNumber);
// 					printf(" -> ");
// 					printVector(h_swarm[s].repository.getSolution(p).decisionVector, decisionNumber);
// 				}
// 			}
// 			if(h_swarm[0].repository.archiverUsed){
// 				printf("\nit: %d\n", iteration);
// 				exit(0);
// 			}else
// 				printf("\n%d",h_swarm[0].repository.getActualSize());

			//mostrador(iteration);
// 			if(!h_diversityPhase){
// 				printf("\n");
// 				for(int i=0;i<swarmNumber;i++){
// 					printf(" %d", h_swarm[i].repository.particlesEntered);
// 				}
// 			}
			//printf("\n%d ",h_initialSwarm.repository.getActualSize());
			
// 			printf("\n - %d repInicial: %d\n", iteration, h_initialSwarm.repository.getActualSize());
// 			for(int s=0;s<swarmNumber;s++)
// 				printf("swarm %d - %d\n",s,h_swarm[s].repository.getActualSize());
		}

		//printf("\nCuda error status: %s\n",cudaGetErrorString(cudaGetLastError()));
		const char* lastError=cudaGetErrorString(cudaGetLastError());
		if(strcmp(lastError, "no error")){
			printf("\nCuda error status: %s\n", lastError);
			exit(1);
		}else
			printf("\n");
		
		repFinal.initialize();
		for(int s=0;s<swarmNumber;s++){
			repFinal.add(h_swarm[s].repository);
			//printf("\n %d particles", repFinal.getActualSize());
		}
		
 		repFinal.organize();
 		shellSort(repFinal.getSolutions(), 0, repFinal.getActualSize()); //sort the repository according to the first objective before it is shown (only a cosmetic step)

		//print the solutions found
		for(int p=0;p<repFinal.getActualSize();p++){
			//printVector(front[p].decisionVector, decisionNumber);
			//printf(" -> ");
			//printVector(repUnico.getSolution(p).objectiveVector, objectiveNumber);
			//printf("\n");
			printVectorToFile(repFinal.getSolution(p).decisionVector, decisionNumber, _s);
			printVectorToFile(repFinal.getSolution(p).objectiveVector, objectiveNumber, _f);
		}
		insertBlankLine(_s);
		insertBlankLine(_f);
		if(!strncmp(algorithm.value, "hmopso", 6))
			insertBlankLine(_p);
	}
	return 0;
}

//*********************************************************************************************************************************************************************//

void hmopsoBefore(){
	if(!strncmp(algorithm.value, "hmopso", 6) && h_swarm[0].repository.archiverUsed){
		//seleciona uma combinacao para usar
		double rnd=(rand()/(double)RAND_MAX);
		double sum=0;
		usada=-1;
		for(int i=0;i<combinacoes;i++){ //ROLETA
			sum+=probabilidades[i];
			usada=i;
			if(rnd <= sum)
				break;
		}
		cudaMemcpyFromSymbol(&h_swarm, d_swarm, sizeof(Swarm)*swarmNumber, 0, cudaMemcpyDeviceToHost);
		switch(usada){
			case 0:
				sprintf(h_swarm[0].repository.archiverType.value, "ideal");//ideal
				for(int i=0;i<swarmSize;i++)
					sprintf(h_swarm[0].particles[i].leaderType.value, "cd");//crowding distance
			break;
			case 1:
				sprintf(h_swarm[0].repository.archiverType.value, "ideal");//ideal
				for(int i=0;i<swarmSize;i++)
					sprintf(h_swarm[0].particles[i].leaderType.value, "nwsum");//nwsum
			break;
			case 2:
				sprintf(h_swarm[0].repository.archiverType.value, "ideal");//ideal
				for(int i=0;i<swarmSize;i++)
					sprintf(h_swarm[0].particles[i].leaderType.value, "sigma");//sigma
			break;
			case 3:
				sprintf(h_swarm[0].repository.archiverType.value, "mga");//mga 
				for(int i=0;i<swarmSize;i++)
					sprintf(h_swarm[0].particles[i].leaderType.value, "cd");//crowding distance
			break;
			case 4:
				sprintf(h_swarm[0].repository.archiverType.value, "mga");//mga
				for(int i=0;i<swarmSize;i++)
					sprintf(h_swarm[0].particles[i].leaderType.value, "nwsum");//nwsum
			break;
			case 5:
				sprintf(h_swarm[0].repository.archiverType.value, "mga");//mga
				for(int i=0;i<swarmSize;i++)
					sprintf(h_swarm[0].particles[i].leaderType.value, "sigma");//sigma
			break;
			case 6:
				sprintf(h_swarm[0].repository.archiverType.value, "cd");//crowding distance
				for(int i=0;i<swarmSize;i++)
					sprintf(h_swarm[0].particles[i].leaderType.value, "cd");//crowding distance
			break;
			case 7:
				sprintf(h_swarm[0].repository.archiverType.value, "cd");//crowding distance
				for(int i=0;i<swarmSize;i++)
					sprintf(h_swarm[0].particles[i].leaderType.value, "nwsum");//nwsum
			break;
			case 8:
				sprintf(h_swarm[0].repository.archiverType.value, "cd");//crowding distance
				for(int i=0;i<swarmSize;i++)
					sprintf(h_swarm[0].particles[i].leaderType.value, "sigma");//sigma
			break;
			default:
				printf("Error on switch");
				exit(1);
			break;
		}
		memcpy(&repPrevious, &h_swarm[0].repository, sizeof(Repository));
		cudaMemcpyToSymbol(d_swarm, &h_swarm, sizeof(Swarm)*swarmNumber, 0, cudaMemcpyHostToDevice);
	}	
}

void hmopsoAfter(){
	if(!strncmp(algorithm.value, "hmopso", 6) && usada != -1){
		double vectmp[combinacoes+1];
		for(int i=0;i<combinacoes;i++)
			vectmp[i]=probabilidades[i];
		vectmp[combinacoes]=usada;
		printVectorToFile(vectmp, combinacoes+1, _p);
		//double pMinima=0.05;//probabilidade minima
		//double pMinima=0.01;//probabilidade minima
		double pMinima=0.005;//probabilidade minima
		
		//atualiza as probabilidades
		double smallerPositions[objectiveNumber], largerPositions[objectiveNumber];
		calculateLargerSmallerVectors(repPrevious, h_swarm[0].repository, reference, tamRef, smallerPositions, largerPositions);
		
		double R2_ant=calcularR2(repPrevious, smallerPositions, largerPositions, reference, tamRef);
		//R2=calcularR2(h_swarm[0].repository.getSolutions(), h_swarm[0].repository.getActualSize(), objectiveNumber, reference, tamRef);
		double R2=calcularR2(h_swarm[0].repository, smallerPositions, largerPositions, reference, tamRef);
		
		double incremento=(1.0/combinacoes)/10;
		////double incremento=(1.0/combinacoes)/5;
		//double incremento=fabs(R2-R2_ant)*10;
		
		if(R2_ant >= R2){ //melhorou
			probabilidades[usada]+=incremento;
			for(int i=0;i<combinacoes;i++){
				if((probabilidades[i]-(incremento/combinacoes)) > pMinima)
				//if((probabilidades[i]-(incremento/combinacoes)) > 0)
					probabilidades[i]-=(incremento/combinacoes);
				else
					probabilidades[usada]-=(incremento/combinacoes);
			}
		}else{ //piorou
		//if(R2_ant < R2){ //piorou
			if(probabilidades[usada]-incremento < pMinima){
				incremento=probabilidades[usada]-pMinima;
				probabilidades[usada]=pMinima;
			}else
				probabilidades[usada]-=incremento;
			for(int i=0;i<combinacoes;i++){
				if((probabilidades[i]+(incremento/combinacoes)) < 1-(pMinima*(combinacoes-1)) ){
				//if((probabilidades[i]+(incremento/combinacoes)) < pMinima*(combinacoes-1)){ //nunca entra
				//if((probabilidades[i]+(incremento/combinacoes)) < 1) //passa de 1
					probabilidades[i]+=(incremento/combinacoes);
				}else{
					probabilidades[usada]+=(incremento/combinacoes);
				}
			}
			//se piorou, restaura a copia do repositorio (only improving)
			memcpy(&h_swarm[0].repository, &repPrevious, sizeof(Repository));
			cudaMemcpyToSymbol(d_swarm, &h_swarm, sizeof(Swarm)*swarmNumber, 0, cudaMemcpyHostToDevice);
			
// 			//mostra
// 			printf("\n%f\t", R2-R2_ant);
// 			double s=0;
// 			for(int i=0;i<combinacoes;i++){
// 				//printf("%.2f%% ",probabilidades[i]*100.0);
// 				printf("%f ",probabilidades[i]);
// 				s+=probabilidades[i];
// 			}
// 			printf("(%f) - %d ",s, usada);
// 			
			
		}
	}	
}

void IMultiOperations(int iteration, int run){
	//end of the diversity phase
	if( ( !strncmp(algorithm.value, "imulti", 5) || !strncmp(algorithm.value, "cmulti", 5) ) && iteration == 100 && h_diversityPhase){		
		h_diversityPhase=false; //diversity phase of the IMulti algorithm
		
		if( !strncmp(algorithm.value, "imulti", 5))
			sprintf(archiver.value, "ideal");
		if( !strncmp(algorithm.value, "cmulti", 5))
			strncpy(archiver.value, archSubSwarms, strlen(archSubSwarms));
			//sprintf(archiver.value, archSubSwarms);
		
		//sprintf(archiver.value, "cd");
		//swarmSize=750/swarmNumber;
		swarmSize=subSwarmPopulation;
		
		if(swarmSize > maxSwarmSize){
			printf("\nError! Swarm size is higher than the maximum allowed\n");
			exit(1);
		}

		cudaInitializeParticles <<< nBlocks, blockSize >>> (swarmSize, swarmNumber, devStates, run, problem, objectiveNumber, decisionNumber, h_diversityPhase, leader);//initialize the particles in paralell
		cudaMemcpyFromSymbol(&h_swarm, d_swarm, sizeof(Swarm)*swarmNumber, 0, cudaMemcpyDeviceToHost);

		//start the population in the defined bounds
		for(int s=0;s<swarmNumber;s++){
			h_swarm[s].repository.initialize(archiver, repositorySize);
		}
		if(h_initialSwarm.repository.getActualSize() < swarmNumber){
			printf("\nERROR! THE REPOSITORY ONLY CONTAINS %d PARTICLES\n", h_initialSwarm.repository.getActualSize());
			for(int i=0;i<h_initialSwarm.repository.getActualSize();i++){
				printVector(h_initialSwarm.repository.getSolution(i).objectiveVector, objectiveNumber);
				printf("\n");
			}
			exit(1);
		}else
			KMeans(h_initialSwarm.repository.getSolutions(), h_initialSwarm.repository.getActualSize(), h_swarm, swarmNumber);
		cudaMemcpyToSymbol(d_swarm, &h_swarm, sizeof(Swarm)*swarmNumber, 0, cudaMemcpyHostToDevice);
	}
	//partitioning iteration
	if( ( !strncmp(algorithm.value, "imulti", 5) || !strncmp(algorithm.value, "cmulti", 5) ) && (!h_diversityPhase && ((iteration-100) % ( (maxIterations-100)/numParticoes) ) == 0) && (iteration>0) ){
		range+=incremento; //calculate new range
		
		repFinal.initialize();
		for(int s=0;s<swarmNumber;s++){
			repFinal.add(h_swarm[s].repository);
			//printf("\n %d particles", repIntermediate.getActualSize());
		}
		
		printf("\npartitioning iteration - %d\n",repFinal.getActualSize());

		//restart the population in the new defined bounds
		cudaInitializeParticles <<< nBlocks, blockSize >>> (swarmSize, swarmNumber, devStates, run, problem, objectiveNumber, decisionNumber, h_diversityPhase, leader);//initialize the particles in paralell
		cudaMemcpyFromSymbol(&h_swarm, d_swarm, sizeof(Swarm)*swarmNumber, 0, cudaMemcpyDeviceToHost);
		
		for(int s=0;s<swarmNumber;s++)
			h_swarm[s].repository.clear();
		if(repFinal.getActualSize() < swarmNumber){
			printf("\nERROR! THE REPOSITORY ONLY CONTAINS %d PARTICLES\n", repFinal.getActualSize());
			for(int i=0;i<repFinal.getActualSize();i++){
				printVector(repFinal.getSolution(i).objectiveVector, objectiveNumber);
				printf("\n");
			}
			exit(1);
		}else
			KMeans(repFinal.getSolutions(), repFinal.getActualSize(), h_swarm, swarmNumber);
		
		cudaMemcpyToSymbol(d_swarm, &h_swarm, sizeof(Swarm)*swarmNumber, 0, cudaMemcpyHostToDevice);
	}
	
	//normal c-multi operations
	if(!strncmp(algorithm.value, "cmulti", 5) && !h_diversityPhase){
		cudaMemcpyFromSymbol(&h_swarm, d_swarm, sizeof(Swarm)*swarmNumber, 0, cudaMemcpyDeviceToHost);		
		//using eda to generate the new population
		for(int s=0;s<swarmNumber;s++){
			
			
			for(int p=0;p<swarmSize;p++){
				if(vectorNan(h_swarm[s].particles[p].solution.decisionVector, decisionNumber)){
					printf("\n ERROR! NaN on decision vector before\n");
					exit(1);
				}
				if(vectorNan(h_swarm[s].particles[p].solution.objectiveVector, objectiveNumber)){
					printf("\n ERROR! NaN on objective vector before\n");
					exit(1);
				}
			}
			
			
			
			if(h_swarm[s].repository.getActualSize() > 2){ //if there is not enough particles, the solutions are updated normally (not overriten)
				eda(h_swarm[s], swarmSize, init);
				if(init) init=false;
			}
			
			
			for(int p=0;p<swarmSize;p++){
				if(vectorNan(h_swarm[s].particles[p].solution.objectiveVector, objectiveNumber)){
					printf("\n ERROR! NaN on objective vector after\n");
					exit(1);
				}
				if(vectorNan(h_swarm[s].particles[p].solution.decisionVector, decisionNumber)){
					printf("\n ERROR! NaN on decision vector after\n");
					exit(1);
				}
			}
			
		}
		cudaMemcpyToSymbol(d_swarm, &h_swarm, sizeof(Swarm)*swarmNumber, 0, cudaMemcpyHostToDevice);
	}
}

__global__ void cudaInitializeParticles(int swarmSize, int swarmNumber, curandState* devStates, int run, str problem, int objectiveNumber, int decisionNumber, bool diversityPhase, str leader){
	int idx = blockIdx.x*blockDim.x + threadIdx.x;
	if(idx==0){
		sigmaSize=(int)combination(objectiveNumber, 2);
		d_objectiveNumber=objectiveNumber;
		d_decisionNumber=decisionNumber;
	}
	
	if(idx<swarmSize*swarmNumber){
		if(run==0) //avoid the same results in each independent run
			curand_init ( clock(), idx, 0, &devStates[idx] );
		d_diversityPhase=diversityPhase;
		
		if(diversityPhase){
			if(idx<swarmSize){
				d_initialSwarm.particles[idx].leaderType=leader;
				d_initialSwarm.particles[idx].initialize(problem,devStates ,objectiveNumber,decisionNumber);
			}
		}else{
			d_swarm[idx%swarmNumber].particles[idx/swarmNumber].leaderType=leader;
			d_swarm[idx%swarmNumber].particles[idx/swarmNumber].initialize(problem,devStates, objectiveNumber, decisionNumber);
		}
	}
}

//initialize the repository with the first non-dominated solutions
void initializeRepository(){
	for(int s=0;s<swarmNumber;s++) //clear all the reposotories
		h_swarm[s].repository.clear();
	
	if(h_diversityPhase){
		cudaMemcpyFromSymbol(&h_initialSwarm, d_initialSwarm, sizeof(Swarm), 0, cudaMemcpyDeviceToHost);
		updateLargerSmallerVectors(h_initialSwarm, swarmSize);
		h_initialSwarm.repository.initialize(archiver, repositorySize);
		for(int p=0;p<swarmSize;p++)
			h_initialSwarm.repository.add(h_initialSwarm.particles[p].solution); //tries to insert the solutions in the repository
		h_initialSwarm.repository.organize();
		if(!strncmp(h_initialSwarm.particles[0].leaderType.value, "cd", 2) || !strncmp(algorithm.value, "hmopso", 6)) updateCrowdingDistances(h_initialSwarm.repository.getSolutions(), h_initialSwarm.repository.getActualSize());
		cudaMemcpyToSymbol(d_initialSwarm, &h_initialSwarm, sizeof(Swarm), 0, cudaMemcpyHostToDevice);
	}else{
		cudaMemcpyFromSymbol(&h_swarm, d_swarm, sizeof(Swarm)*swarmNumber, 0, cudaMemcpyDeviceToHost);
		for(int s=0;s<swarmNumber;s++){
			updateLargerSmallerVectors(h_swarm[s], swarmSize);
			h_swarm[s].repository.initialize(archiver, repositorySize);
			for(int p=0;p<swarmSize;p++){
				h_swarm[s].repository.add(h_swarm[s].particles[p].solution); //tries to insert the solutions in the repository
			}
			h_swarm[s].repository.organize();
			if(!strncmp(h_swarm[s].particles[0].leaderType.value, "cd", 2) || !strncmp(algorithm.value, "hmopso", 6)) updateCrowdingDistances(h_swarm[s].repository.getSolutions(), h_swarm[s].repository.getActualSize());
		}
		cudaMemcpyToSymbol(d_swarm, &h_swarm, sizeof(Swarm)*swarmNumber, 0, cudaMemcpyHostToDevice);
	}
}

//choose the global leaders according to the strategy defined in the begining of this file
//param - a set of solutions (with no "holes") representing the actual pareto front
__global__ void chooseGlobalLeaders(int swarmSize, int swarmNumber, curandState* devStates){
	int idx = blockIdx.x*blockDim.x + threadIdx.x;
	
	if(idx<swarmSize*swarmNumber){
		if(d_diversityPhase){
			if(idx<swarmSize)
				d_initialSwarm.particles[idx].chooseGlobalLeader(d_initialSwarm.repository.getSolutions(), d_initialSwarm.repository.getActualSize(), d_initialSwarm.repository.smallerPositions, d_initialSwarm.repository.largerPositions, devStates);
		}else
			d_swarm[idx%swarmNumber].particles[idx/swarmNumber].chooseGlobalLeader(d_swarm[idx%swarmNumber].repository.getSolutions(), d_swarm[idx%swarmNumber].repository.getActualSize(), d_swarm[idx%swarmNumber].repository.smallerPositions, d_swarm[idx%swarmNumber].repository.largerPositions, devStates);
	}
}

//calculate the velocity of the particles
__global__ void calculateVelocity(int swarmSize, int swarmNumber, curandState* devStates){
	int idx = blockIdx.x*blockDim.x + threadIdx.x;
	
	if(idx<swarmSize*swarmNumber){	
		if(d_diversityPhase){
			if(idx<swarmSize)
				d_initialSwarm.particles[idx].computeSpeed(devStates);
		}else
			d_swarm[idx%swarmNumber].particles[idx/swarmNumber].computeSpeed(devStates);
	}
}

//update the position of the particles based on its velocity and actual positions
__global__ void updatePosition(int swarmSize,  int swarmNumber){
	int idx = blockIdx.x*blockDim.x + threadIdx.x;
	
	if(idx<swarmSize*swarmNumber){
		if(d_diversityPhase){
			if(idx<swarmSize)
				d_initialSwarm.particles[idx].updatePosition();
		}else
			d_swarm[idx%swarmNumber].particles[idx/swarmNumber].updatePosition();
	}
}

//apply the turbulence factor in a percentage of the swarm defined in the begining of this file
__global__ void turbulence(int swarmSize, int swarmNumber, curandState* devStates, int mutationIndice){
	int idx = blockIdx.x*blockDim.x + threadIdx.x;
	
	//JMetal
	if(idx<swarmSize*swarmNumber){
		curandState localState = devStates[idx];
		if(d_diversityPhase){
			if(idx % 6 == 0 && idx<swarmSize)
				d_initialSwarm.particles[idx].turbulence(devStates);
		}else{
			if(idx/swarmNumber % 6 == 0){
				d_swarm[idx%swarmNumber].particles[idx/swarmNumber].turbulence(devStates);
			}
		}
		devStates[idx] = localState;
	}
}

//evaluate the particles according to its actual position
__global__ void evaluation(int swarmSize, int swarmNumber, double range, str leader, str algorithm){
	int idx = blockIdx.x*blockDim.x + threadIdx.x;
	
	if(idx<swarmSize*swarmNumber){
		if(d_diversityPhase){
			if(idx<swarmSize)
				d_initialSwarm.particles[idx].solution.evaluate();
		}else{
			if(!cudaStrncmp(algorithm.value, "imulti", 5) || !cudaStrncmp(algorithm.value, "cmulti", 5))
				d_swarm[idx%swarmNumber].particles[idx/swarmNumber].truncatePositionIMulti(d_swarm[idx%swarmNumber].centroid, range, d_decisionNumber, d_inferiorPositionLimit, d_superiorPositionLimit);
			d_swarm[idx%swarmNumber].particles[idx/swarmNumber].solution.evaluate();
		}
	}
}


//update the repository with the new non-dominated solutions
void updateRepository(){
	if(h_diversityPhase){
		cudaMemcpyFromSymbol(&h_initialSwarm, d_initialSwarm, sizeof(Swarm), 0, cudaMemcpyDeviceToHost);
		updateLargerSmallerVectors(h_initialSwarm, swarmSize);
		for(int p=0;p<swarmSize;p++){
			h_initialSwarm.repository.add(h_initialSwarm.particles[p].solution); //tries to insert the solutions in the repository
		}
		h_initialSwarm.repository.organize();
		if(!strncmp(h_initialSwarm.particles[0].leaderType.value, "cd", 2) || !strncmp(algorithm.value, "hmopso", 6)) updateCrowdingDistances(h_initialSwarm.repository.getSolutions(), h_initialSwarm.repository.getActualSize());
		cudaMemcpyToSymbol(d_initialSwarm, &h_initialSwarm, sizeof(Swarm), 0, cudaMemcpyHostToDevice);
	}else{
		cudaMemcpyFromSymbol(&h_swarm, d_swarm, sizeof(Swarm)*swarmNumber, 0, cudaMemcpyDeviceToHost);		
		for(int s=0;s<swarmNumber;s++){
			h_swarm[s].repository.particlesEntered=0; //initialize the counters of numbers of particles that enters in the repository per iteration
			updateLargerSmallerVectors(h_swarm[s], swarmSize);
			for(int p=0;p<swarmSize;p++){
				h_swarm[s].repository.add(h_swarm[s].particles[p].solution); //tries to insert the solutions in the repository
			}
			h_swarm[s].repository.organize();
			if(!strncmp(h_swarm[s].particles[0].leaderType.value, "cd", 2) || !strncmp(algorithm.value, "hmopso", 6)) updateCrowdingDistances(h_swarm[s].repository.getSolutions(), h_swarm[s].repository.getActualSize());
		}
		cudaMemcpyToSymbol(d_swarm, &h_swarm, sizeof(Swarm)*swarmNumber, 0, cudaMemcpyHostToDevice);
	}
}

//update the local leader of the particles
//param - a set of solutions (with no "holes") representing the actual pareto front
__global__ void updateParticlesMemory(int swarmSize, int swarmNumber, curandState* devStates){
	int idx = blockIdx.x*blockDim.x + threadIdx.x;
	
	if(idx<swarmSize*swarmNumber){
		if(d_diversityPhase){
			if(idx<swarmSize)
				d_initialSwarm.particles[idx].updateLocalLeader(devStates);
		}else
			d_swarm[idx%swarmNumber].particles[idx/swarmNumber].updateLocalLeader(devStates);
	}
}

//show the progress of the optimization
//param - number of the actual iteration of the algorithm
void mostrador(int iteration){
	if( (int)fmod( (iteration+1), (maxIterations/20)) == 0){
		printf("%d%% ", (int)floor( ( (iteration+1.0)/maxIterations) *100) );
	}
}

void readParameters(int argc, char *inputFile){
	if(argc < 2){
		printf("uso: cuda_mopso<parameters file>\n\n");
		exit(1);
	}
	
	char s[100];
	FILE *stream = fopen(inputFile, "r");
	if( stream == NULL ){
		printf("\nFail to Open Parameters File!!\n");
		exit(1);
	}
    while(fgets(s,100,stream)){
		if ((s[0]) && (s[0] != '%')) { //if the line is not a comment

			for(int i=0;i<strlen(s);i++) //the for is to remove the line break
				if(s[i] == '\n')
					s[i]=' ';

			char* var = strtok(s,"=");
			char* value = strtok(NULL,"=");

			//printf("var: %s = value: %s\n", var, value);

			if(!strncmp(var, "outName", 7))
				strncpy(outputFileName, value, strlen(value)-1);
			if(!strncmp(var, "problem", 7))
				strncpy(problem.value, value, strlen(value)-1);
			if(!strncmp(var, "leader", 6))
				strncpy(leader.value, value, strlen(value)-1);
			if(!strncmp(var, "archiver", 8))
				strncpy(archiver.value, value, strlen(value)-1);
			if(!strncmp(var, "objectiveNumber", 15))
				objectiveNumber=atoi(value);
			if(!strncmp(var, "population", 10))
				swarmSize=atoi(value);
			if(!strncmp(var, "repository", 10))
				repositorySize=atoi(value);
			if(!strncmp(var, "swarms", 6))
				swarmNumber=atoi(value);
			if(!strncmp(var, "iterations", 10))
				maxIterations=atoi(value);
			if(!strncmp(var, "runs", 4))
				numExec=atoi(value);
			if(!strncmp(var, "algorithm", 9))
				strncpy(algorithm.value, value, strlen(value)-1);
			if(!strncmp(var, "partIterations", 14))
				numParticoes=atoi(value);
			if(!strncmp(var, "archSubSwarms", 15))
				strncpy(archSubSwarms, value, strlen(value)-1);
			
			
// 			else{
// 				printf("\nUnknown parameter %s with value %s\n", var, value);
// 				exit(1);
// 			}
		}
	}
	fclose(stream);
	if(!strncmp(problem.value, "dtlz", 4)){
		if(!strncmp(problem.value, "dtlz1", 5))
			decisionNumber=objectiveNumber+5-1; //if dtlz1, k=5
		else
			if(!strncmp(problem.value, "dtlz7", 5))
				decisionNumber=objectiveNumber+20-1; //if dtlz7, k=20
			else
				decisionNumber=objectiveNumber+10-1; // otherwise, k=10
				
		for (int var = 0; var < decisionNumber; var++) {
			inferiorPositionLimit[var] = 0;
			superiorPositionLimit[var] = 1;
		}
		
	}
		
	if(!strncmp(problem.value, "wfg", 3)){//if the problem is from the WFG family
		//l=20 (distance related), k=4 (position related) if M=2, otherwise k=2*(M-1) //from the WFG readme file
		if(objectiveNumber == 2)
			decisionNumber=20+4;
		else
			decisionNumber=20+(2*(objectiveNumber-1));
		
		for (int var = 0; var < decisionNumber; var++) {
			inferiorPositionLimit[var] = 0;
			superiorPositionLimit[var] = 2 * (var + 1);
		}
	}
	
	cudaMemcpyToSymbol(d_inferiorPositionLimit, &inferiorPositionLimit, sizeof(double)*decisionNumber, 0, cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(d_superiorPositionLimit, &superiorPositionLimit, sizeof(double)*decisionNumber, 0, cudaMemcpyHostToDevice);
	
	double deltaMax_[decisionNumber];
	double deltaMin_[decisionNumber];
	
	for (int i = 0; i < decisionNumber; i++) {
		deltaMax_[i] = (superiorPositionLimit[i] - inferiorPositionLimit[i]) / 2.0;
		deltaMin_[i] = -deltaMax_[i];
	}	
	cudaMemcpyToSymbol(deltaMax, &deltaMax_, sizeof(double)*decisionNumber, 0, cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(deltaMin, &deltaMin_, sizeof(double)*decisionNumber, 0, cudaMemcpyHostToDevice);

	if(swarmNumber > maxSwarmNumber){
		printf("\nError! Swarm number is higher than the maximum allowed\n");
		exit(1);
	}
	if(objectiveNumber > maxObjectiveNumber){
		printf("\nError! Objective number is higher than the maximum allowed\n");
		exit(1);
	}
	if(swarmSize > maxSwarmSize){
		printf("\nError! Swarm size is higher than the maximum allowed\n");
		exit(1);
	}
	if(repositorySize > maxRepositorySize){
		printf("\nError! Repository size is higher than the maximum allowed\n");
		exit(1);
	}
	//nBlocks - number of times in paralell the code will run
	blockSize = 8;
	nBlocks = (swarmSize*swarmNumber)/blockSize + ((swarmSize*swarmNumber)%blockSize == 0?0:1);
}
