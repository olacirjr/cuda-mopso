class Particle{
public:

	Solution solution; //solution contained by this particle
	double velocity[maxDecisionNumber]; //velocity of this particle
	Solution localBest; //local best solution
	Solution globalBest; //global best solution
	str leaderType;

// 	double VELOCITY_REDUCTION;
// 	double MAX_MUT;

	//initialize the particle with random values
	__device__ void initialize(str problem, curandState* devStates, int On, int Dn);
	//compute the speed of the particle
	__device__ void computeSpeed(curandState* devStates);
	//update the position according to the velocity
	__device__ void updatePosition();
	//choose the global leader
	//params - a set of solutions representing the actual pareto front
	//		 - the code of the strategy for selecting the leader (set in the main.cpp)
	//		 - the number of solutions composing the given front
	//		 - the current status of the random number generator
	__device__ void chooseGlobalLeader(Solution* repository, int repActualSize, double* smallerPositions, double* largerPositions, curandState* devStates);
	//strategy to choose the global leader - chooses the leader randomly
	//params - a set of solutions representing the actual pareto front
	//		 - the number of solutions composing the given front
	//		 - the current status of the random number generator
	__device__ void randomLeader(Solution* repository, int repActualSize, curandState* devStates);
	//strategy to choose the global leader - chooses the leader based in the crowding distance
	//params - a set of solutions representing the actual pareto front
	//		 - the number of solutions composing the given front
	//		 - the current status of the random number generator
	__device__ void crowdingDistanceLeader(Solution* repository, int repActualSize, curandState* devStates);
	//The method NWSum of leader selection
	//params - a set of solutions representing the actual pareto front
	//		 - the number of solutions composing the given front
	__device__ void NWSumLeader(Solution* repository, int repActualSize, double* smallerPositions, double* largerPositions);
	//The method Sigma of leader selection
	//params - a set of solutions representing the actual pareto front
	//		 - the number of solutions composing the given front
	__device__ void SigmaLeader(Solution* repository, int repActualSize, double* smallerPositions, double* largerPositions);
	//update the local best solution
	__device__ void updateLocalLeader(curandState* devStates);
	//reduce the speed if the particle exceed the bounds
	//__device__ void reduceSpeed();
	//the turbulence operator
	__device__ void turbulence(curandState* devStates);
	//keep the position in the swarm specified bounds in IMulti algorithm
	__host__ __device__ bool truncatePositionIMulti(double* centroid, double range, int decisionNumber, double* inferiorLimit, double* superiorLimit);
	
	//__host__ __device__ void truncatePositon();

private:

	double c1;
	double c2;

// 	double MAX_PHI;
// 	double MAX_OMEGA;
// 	double INERTIA;
// 	double superiorVelocityLimit;
// 	double inferiorVelocityLimit;

	//initialize the velocity randomly
		__device__ void initializeVelocity(curandState* devStates);
		//update omega value randomly
		__device__ double getOmega(curandState* devStates);
		//update phi value randomly
		__device__ double getPhi(curandState* devStates);
		//update c1 value randomly
		__device__ double getC1(curandState* devStates);
		//update C2 value randomly
		__device__ double getC2(curandState* devStates);
		//calculate Fi value
		__device__ double getFi();

};
//constructor initialize the variables
//Particle::Particle(int On, int Dn){
//	objectiveNumber=On;
//	decisionNumber=Dn;
////	solution = new Solution(objectiveNumber, decisionNumber);
//	velocity = new double[decisionNumber];
//	position = new double[decisionNumber];
////	localBest = new Solution(objectiveNumber, decisionNumber);
////	globalBest = new Solution(objectiveNumber, decisionNumber);
//	MAX_PHI=1;
//	MAX_OMEGA=0.8;
//	limiteSuperiorVelocidade=5;
//	limiteInferiorVelocidade=-5;
//}
__device__ void scalarVectorMultiply(double scalar, double* vector, double* C, int length);
__device__ __host__ void vectorSum(double* A, double* B, double* C, int length);
__device__ __host__ void vectorSub(double* A, double* B, double* C, int length);
__device__ double calculateNWSum(Solution solution, Solution GBest, double* smallerPositions, double* largerPositions);
__device__ __host__ double calculateEuclideanDistance(double* vector1, double* vector2, int size);
__device__ __host__ int dominance(double sol1[], double sol2[], int objectiveNumber);

//initialize the particle with random values
__device__ void Particle::initialize(str problem, curandState* devStates, int On, int Dn){
	//solution.decisionNumber=decisionNumber=Dn;
	//solution.objectiveNumber=objectiveNumber=On;
	solution.initialize(problem, devStates); //initialize the position with random values in range [0,1]

//	localBest.decisionVector= new double[decisionNumber];
//	localBest.objectiveVector= new double[objectiveNumber];

//	globalBest.decisionVector= new double[decisionNumber];
//	globalBest.objectiveVector= new double[objectiveNumber];

	//localBest.deepCopy(solution); //this solution is the best of the particle since now
	memcpy(&localBest, &solution, sizeof(Solution));

	initializeVelocity(devStates); //initialize the velocity with random values in range [0,1]
}

__device__ double velocityConstriction(double v, int var){
    double result=v;
	double dmax = deltaMax[var];
    double dmin = deltaMin[var];
    
    
    if (v > dmax) {
      result = dmax;
    }
    if (v < dmin) {
      result = dmin;
    }
    return result;
  } // velocityConstriction
  
    // constriction coefficient (M. Clerc)
__device__ double constrictionCoefficient(double c1, double c2) {
    double rho = c1 + c2;
    //rho = 1.0 ;
    if (rho <= 4) {
      return 1.0;
    } else {
      return 2.0 / (2.0 - rho - sqrt( (rho*rho) - 4.0 * rho));
    }
  } // constrictionCoefficient
  
//compute the speed of the particle
__device__ void Particle::computeSpeed(curandState* devStates){
// 	//v=X{w.v+c1.r1.[lbest-x]+c2.r2.[gbest-x]}
	
// 	double step1[maxDecisionNumber];//step1=w.v
// 	//scalarVectorMultiply(getOmega(devStates), velocity, step1, decisionNumber); //algoritmo do andré
// 	scalarVectorMultiply(INERTIA, velocity, step1, decisionNumber);
// 
// 	double step2[maxDecisionNumber];//step2=(c1.r1).(lbest-x)
// 	double tmp[maxDecisionNumber];//lbest-x
// 	vectorSub(localBest.decisionVector, solution.decisionVector,tmp,decisionNumber);
// 	scalarVectorMultiply(C1*r1, tmp, step2, decisionNumber); //the same phi (random) value for all dimensions
// 
// 	double step3[maxDecisionNumber];//step3=(c2.r2).(gbest-x)
// 	vectorSub(globalBest.decisionVector, solution.decisionVector,tmp,decisionNumber);
// 	scalarVectorMultiply(C2*r2, tmp, step3, decisionNumber); //the same phi (random) value for all dimensions
// 
// 	double step4[maxDecisionNumber];//step4=w.v+(c1.r1).(lbest-x)+(c2.r2).(gbest-x)
// 	vectorSum(step2, step3,tmp,decisionNumber);
// 	vectorSum(step1, tmp, step4, decisionNumber);
// 
// 	double fi = getFi();// c1+c2 or 1
// 	double root=(fi*fi) - (4*fi);
// 	double X=1; //calculation of SMPSO constriction factor according to jmetal
// 	if(root>=0)
// 		X=2.0/(2-fi-sqrt(root));
// 
// 	scalarVectorMultiply(X, step4, velocity, decisionNumber);
// 
// 	double deltai = ((superiorVelocityLimit-inferiorVelocityLimit)/2.0);
// 	for(int i=0;i<decisionNumber;i++){
// 		if(velocity[i]>deltai)
// 			velocity[i]=deltai;
// 		if(velocity[i]<(deltai*-1))
// 			velocity[i]=deltai*-1;
// 	}
	
	double r1 = getPhi(devStates);
	double r2 = getPhi(devStates);
	double C1 = getC1(devStates);
	double C2 = getC2(devStates);
	//	
	for (int var = 0; var < d_decisionNumber; var++) {
	//Computing the velocity of this particle
		velocity[var]=velocityConstriction( constrictionCoefficient(C1, C2) * (INERTIA *	velocity[var] +
		C1 * r1 * (localBest.decisionVector[var] - solution.decisionVector[var]) +
		C2 * r2 * (globalBest.decisionVector[var]- solution.decisionVector[var])), var);
	}
}

//update the particle position according to the velocity
__device__ void Particle::updatePosition(){
	//double tmp[maxDecisionNumber];
	//vectorSum(solution.decisionVector, velocity, tmp, decisionNumber);
	for(int i=0;i<d_decisionNumber;i++){ //deep copying
		//solution.decisionVector[i]=tmp[i];
		solution.decisionVector[i]+=velocity[i];
		
		if (solution.decisionVector[i] < d_inferiorPositionLimit[i]) {
			solution.decisionVector[i]=d_inferiorPositionLimit[i];
			velocity[i] = velocity[i] * -1; //
		}
		if (solution.decisionVector[i] > d_superiorPositionLimit[i]) {
			solution.decisionVector[i]=d_superiorPositionLimit[i];
			velocity[i] = velocity[i] * -1; //
		}
	}
}

//initialize the velocity randomly
__device__ void Particle::initializeVelocity(curandState* devStates){
	//velocity = new double[decisionNumber];
	int idx = blockIdx.x*blockDim.x + threadIdx.x;
	curandState localState = devStates[idx];
	for(int i=0;i<d_decisionNumber;i++)
		//velocity[i]=(rand()/(double)RAND_MAX);
		//velocity[i]=(curand_uniform( &localState)*(d_superiorPositionLimit[i]-d_inferiorPositionLimit[i]) )+d_inferiorPositionLimit[i] ; // Variation of Andre method
		velocity[i]=0.0; //JMetal
		

		devStates[idx] = localState;
}

//update omega value randomly
__device__ double Particle::getOmega(curandState* devStates){
	int idx = blockIdx.x*blockDim.x + threadIdx.x;
	curandState localState = devStates[idx];
	//return fmod((rand()/(double)RAND_MAX),MAX_PHI);
	double value= (double)(curand_uniform(&localState)*MAX_OMEGA);
	devStates[idx] = localState;
	return value;
}

//update phi value randomly
__device__ double Particle::getPhi(curandState* devStates){
	int idx = blockIdx.x*blockDim.x + threadIdx.x;
	curandState localState = devStates[idx];
	//return fmod((rand()/(double)RAND_MAX),MAX_PHI);
	double value= (double)(curand_uniform(&localState)*MAX_PHI);
	devStates[idx] = localState;
	return value;
}

//update c1 value randomly
__device__ double Particle::getC1(curandState* devStates){
	int idx = blockIdx.x*blockDim.x + threadIdx.x;
	curandState localState = devStates[idx];
	//return c1=(rand()/(double)RAND_MAX)+1.5;
	c1=(curand_uniform( &localState)+1.5);
	devStates[idx] = localState;
	return c1;
}

//update c2 value randomly
__device__ double Particle::getC2(curandState* devStates){
	int idx = blockIdx.x*blockDim.x + threadIdx.x;
	curandState localState = devStates[idx];
	//return c2=(rand()/(double)RAND_MAX)+1.5;
	c2=(curand_uniform( &localState)+1.5);
	devStates[idx] = localState;
	return c2;
}

//calculate Fi value
__device__ double Particle::getFi(){
	if(c1+c2 > 4)
		return c1+c2;
	else
		return 1;
}
//choose the global leader randomly
//params - a set of solutions representing the actual pareto front
//		 - the number of solutions composing the given front
//		 - the current status of the random number generator
__device__ void Particle::randomLeader(Solution* repository, int repActualSize, curandState* devStates){
	//globalBest.deepCopy(repository[(rand() % repActualSize)]);

	int idx = blockIdx.x*blockDim.x + threadIdx.x;
	curandState localState = devStates[idx];

	//globalBest=repository[(rand() % repActualSize)];
	//globalBest=repository[(int)(curand_uniform( &localState)*repActualSize)];
	memcpy(&globalBest, &repository[(int)(curand_uniform( &localState)*repActualSize)], sizeof(Solution));

	devStates[idx] = localState;
}
//choose the global leader according to the crowding distance
//params - a set of solutions representing the actual pareto front
//		 - the number of solutions composing the given front
//		 - the current status of the random number generator
__device__ void Particle::crowdingDistanceLeader(Solution* repository, int repActualSize, curandState* devStates){
	//updateCrowdingDistances(repository, repActualSize);

	int idx = blockIdx.x*blockDim.x + threadIdx.x;
	curandState localState = devStates[idx];

	//int rnd1=(rand() % repActualSize);
	//int rnd2=(rand() % repActualSize);
	int rnd1=(int)(curand_uniform( &localState)*repActualSize);
	int rnd2=(int)(curand_uniform( &localState)*repActualSize);

	devStates[idx] = localState;

	if(repository[rnd1].crowdingDistance <0 || repository[rnd2].crowdingDistance<0)
		printf("\nCROWDING DISTANCE LEADER ERROR!!\n");

	if(repository[rnd1].crowdingDistance > repository[rnd2].crowdingDistance)//the solution in the less crowded region (bigger crowding distance) is selected as leader
		//globalBest=repository[rnd1];
		memcpy(&globalBest, &repository[rnd1], sizeof(Solution));
		
	else
		//globalBest=repository[rnd2];
		memcpy(&globalBest, &repository[rnd2], sizeof(Solution));
}
//The method NWSum of leader selection
//params - a set of solutions representing the actual pareto front
//		 - the number of solutions composing the given front
__device__ void Particle::NWSumLeader(Solution* repository, int repActualSize, double* smallerPositions, double* largerPositions){

	double betterNWSum = 0;
	Solution GBest;
	for(int p=0;p<repActualSize;p++) {
		//calculate the NWSum metric according to the actual solution and the solutions from the repository
		double NWSum= calculateNWSum(solution, repository[p], smallerPositions, largerPositions); //in util.cpp

		//The higher the NWSum the better
		//if this NWSum is higher than the previous, replace
		if(NWSum>betterNWSum){
			betterNWSum= NWSum;
			GBest = repository[p];
		}
	}
	//globalBest=GBest;
	memcpy(&globalBest, &GBest, sizeof(Solution));
}
//The method Sigma of leader selection
//params - a set of solutions representing the actual pareto front
//		 - the number of solutions composing the given front
__device__ void Particle::SigmaLeader(Solution* repository, int repActualSize, double* smallerPositions, double* largerPositions){
	double betterSigma = MAXDOUBLE;
	Solution GBest;
	GBest.calculateSigmaVector(smallerPositions, largerPositions);
	for(int p=0;p<repActualSize;p++) {
		solution.calculateSigmaVector(smallerPositions, largerPositions);
		//calculate the Sigma distance
		double sigmaDistance = calculateEuclideanDistance(solution.sigmaVector, repository[p].sigmaVector, sigmaSize);
// 		if(repository[p].sigmaVector[0]==0){
// 			printf("SIGMA LEADER ERROR!!\n");
// 			for(int i=0;i<solution.sigmaSize;i++)
// 				printf("%e\t",repository[p].sigmaVector[i]);
// 			printf("\n");
// 			for(int i=0;i<objectiveNumber;i++)
// 				printf("%e\t",repository[p].objectiveVector[i]);
// 			printf("\n");
// 		}
		//The smaller the SigmaDistance the better
		if(sigmaDistance<betterSigma){
			betterSigma= sigmaDistance;
			GBest = repository[p];
		}
	}
	//globalBest=GBest;
	memcpy(&globalBest, &GBest, sizeof(Solution));
}
//update the local leader
__device__ void Particle::updateLocalLeader(curandState* devStates){
	//int idx = blockIdx.x*blockDim.x + threadIdx.x;
	//curandState localState = devStates[idx];

	//verify the dominance relation between
	//return 1 if sol1 dominates sol2, -1 if sol2 dominates sol1, 0 if they do not dominate each other, 2 if they are equal
	int tmp=dominance(solution.objectiveVector, localBest.objectiveVector, d_objectiveNumber);
	if(tmp == 1){//if the new position is better then the old
		//localBest.deepCopy(solution); //this solution is the best of the particle since now
		memcpy(&localBest, &solution, sizeof(Solution));
	}else{
		if(tmp==0){ // if the solutions non dominate each other
			//if usado no algoritmo do andré
			//if(curand_uniform( &localState) > 0.5){ //the new solution have 50% chance to replace the old
				//localBest.deepCopy(solution); //this solution is the best of the particle since now
				memcpy(&localBest, &solution, sizeof(Solution));
			//}
		}
	}
	//devStates[idx] = localState;
}

//choose the global leader
//params - a set of solutions representing the actual pareto front
//		 - the code of the strategy for selecting the leader (set in the main.cpp)
//		 - the number of solutions composing the given front
//		 - the current status of the random number generator
__device__ int cudaStrncmp(char* str1, char* str2, int size);
__device__ void Particle::chooseGlobalLeader(Solution* repository, int repActualSize, double* smallerPositions, double* largerPositions, curandState* devStates){
	if(!cudaStrncmp(leaderType.value, "rnd", 3))
		randomLeader(repository, repActualSize, devStates);
	if(!cudaStrncmp(leaderType.value, "cd", 2))
		crowdingDistanceLeader(repository, repActualSize, devStates);
	if(!cudaStrncmp(leaderType.value, "nwsum", 5))
		NWSumLeader(repository, repActualSize, smallerPositions, largerPositions);
	if(!cudaStrncmp(leaderType.value, "sigma", 3))
		SigmaLeader(repository, repActualSize, smallerPositions, largerPositions);
	
	if(cudaStrncmp(leaderType.value, "rnd", 3) && cudaStrncmp(leaderType.value, "cd", 2) && cudaStrncmp(leaderType.value, "nwsum", 5) && cudaStrncmp(leaderType.value, "sigma", 5))
		printf("INVALID LEADER SELECTION METHOD! (%s)\n", leaderType.value);
	
	
// 	switch(leaderType){
// 		case 1:
// 			randomLeader(repository, repActualSize, devStates);
// 			break;
// 		case 2:
// 			crowdingDistanceLeader(repository, repActualSize, devStates);
// 			break;
// 		case 3:
// 			NWSumLeader(repository, repActualSize, smallerPositions, largerPositions);
// 			break;
// 		case 4:
// 			SigmaLeader(repository, repActualSize, smallerPositions, largerPositions);
// 			break;
// 		default:
// 			printf("Invalid leader selection method!\n");
// 			//exit(1);
// 		}
}
// //reduce the speed of the particles when it position exceeds the bounds
// __device__ void Particle::reduceSpeed(){
// 	for(int i=0;i<decisionNumber;i++)
// 		velocity[i] *= VELOCITY_REDUCTION;
// }
// //apply the turbulence operator - Andre
// __device__ void Particle::turbulence(curandState* devStates){
// 	int idx = blockIdx.x*blockDim.x + threadIdx.x;
// 	curandState localState = devStates[idx];
// 	for(int i=0;i<decisionNumber;i++){
// 		double pos = solution.decisionVector[i];
// 		//double prob=(rand()/(double)RAND_MAX);
// 		double prob =curand_uniform( &localState);
// 		double delta;
// 		double mutation_prob=(1.0/decisionNumber);
// 		if(prob<mutation_prob){
// 			//double u =(rand()/(double)RAND_MAX);
// 			double u =curand_uniform( &localState);
// 			if(u < 0.5)
// 				delta=pow(2.0*u,1.0/(decisionNumber+1))-1;
// 			else
// 				delta=1-pow(2.0*(1-u),1.0/(decisionNumber+1));
// 		}else
// 			delta=0;
// 		solution.decisionVector[i]=(pos+delta*MAX_MUT);
// 	}
// 	devStates[idx] = localState;
// }

//apply the turbulence operator - JMetal
__device__ void Particle::turbulence(curandState* devStates){
	double probability=1.0/d_decisionNumber;
	int idx = blockIdx.x*blockDim.x + threadIdx.x;
	curandState localState = devStates[idx];
	
	double rnd, delta1, delta2, mut_pow, deltaq;
	double y, yl, yu, val, xy;
	for (int var=0; var < d_decisionNumber; var++) {
		if (curand_uniform( &localState) <= probability)
		{
			y      = solution.decisionVector[var];
			yl     = d_inferiorPositionLimit[var];//lower bound            
			yu     = d_superiorPositionLimit[var];//upper bound
			delta1 = (y-yl)/(yu-yl);
			delta2 = (yu-y)/(yu-yl);
			rnd = curand_uniform( &localState);
			mut_pow = 1.0/(20.0+1.0);
			if (rnd <= 0.5)
			{
				xy     = 1.0-delta1;
				val    = 2.0*rnd+(1.0-2.0*rnd)*(pow(xy,(20.0+1.0)));
				deltaq =  pow(val,mut_pow) - 1.0;
			}
			else
			{
				xy = 1.0-delta2;
				val = 2.0*(1.0-rnd)+2.0*(rnd-0.5)*(pow(xy,(20.0+1.0)));
				deltaq = 1.0 - (pow(val,mut_pow));
			}
			y = y + deltaq*(yu-yl);
			if (y<yl)
				y = yl;
			if (y>yu)
				y = yu;
			solution.decisionVector[var]= y;                           
		}
	} // for
	devStates[idx] = localState;
}

// //keep the position in the swarm specified bounds in IMulti algorithm
// __host__ __device__ bool Particle::truncatePositionIMulti(double* centroid, double range, int decisionNumber, double* inferiorLimit, double* superiorLimit){ //according to JMetal
// 	bool truncate=false;
// 	
// 	for(int i=0;i<decisionNumber;i++){
// 		double inf = max(inferiorLimit[i], (centroid[i]-range)*(superiorLimit[i]-inferiorLimit[i]));
// 		double sup = min(superiorLimit[i], (centroid[i]+range)*(superiorLimit[i]-inferiorLimit[i]));
// 		if(solution.decisionVector[i]<inf){
// 			solution.decisionVector[i]=inf;
// 			velocity[i] = velocity[i] * -1;
// 			truncate=true;
// 		}
// 		if(solution.decisionVector[i]>sup){
// 			solution.decisionVector[i]=sup;
// 			velocity[i] = velocity[i] * -1;
// 			truncate=true;
// 		}
// 	}
// 	return truncate;
// }
//keep the position in the swarm specified bounds in IMulti algorithm
__host__ __device__ bool Particle::truncatePositionIMulti(double* centroid, double range, int decisionNumber, double* inferiorLimit, double* superiorLimit){ //original do André
	bool truncate=false;
	
	for(int i=0;i<decisionNumber;i++){
		double var_i=solution.decisionVector[i];
		double inf = max(inferiorLimit[i], (centroid[i]-range)*(superiorLimit[i]-inferiorLimit[i]));
		double sup = min(superiorLimit[i], (centroid[i]+range)*(superiorLimit[i]-inferiorLimit[i]));
		if(var_i<inf){
			double part1=(sup-inf);
			double part2=fabs(inf - var_i);
			double part3=fmod(part2,part1);
			double part4=sup-part3;

			solution.decisionVector[i]=part4;
			truncate=true;
		}
		if(var_i>sup){
			double part1=(sup-inf);
			double part2=fabs(var_i-sup);
			double part3=fmod(part2,part1);
			double part4=part3+inf;

			solution.decisionVector[i]=part4;
			truncate=true;
		}
	}
	return truncate;
}



// __host__ __device__ void Particle::truncatePositon(){
// 	//JMetal
// 	for (int var = 0; var < decisionNumber; var++) {
// 		if (solution.decisionVector[var] < solution.inferiorPositionLimit) {
// 			solution.decisionVector[var]=solution.inferiorPositionLimit;
// 			velocity[var] = velocity[var] * -1; //
// 		}
// 		if (solution.decisionVector[var] > solution.superiorPositionLimit) {
// 			solution.decisionVector[var]=solution.superiorPositionLimit;
// 			velocity[var] = velocity[var] * -1; //
// 		}
// 	}
// }
