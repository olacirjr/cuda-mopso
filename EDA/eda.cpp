int	numMixComponents=3; //parametro do aprendizado
int selsize; // numero de elementos selecionados para treinar o modelo
int stringlength; //numero de variaveis de decisao
int	kappa=5; //number of parents
int	saveModel=0; //salvar ou nao o modelo em disco
int	maximumAmountOfClusters=200;  // Maximum number of possible clusters for leader algorithm.
double	leaderThreshold=0.3; //randomized leader algorithm (RLA) with a threshold value of 0.3
int useClusters=1;
int	generation; //contador pra geracao atual
int	popsize; //tamanho da populacao
//double 	**population;
Swarm* swarm;
bool init=true;

//#define GETREAL(INDEX,MEMBER) (population[(MEMBER)][(INDEX)])  // obtem um membro da populacao
//#define GETREALSELECTED(INDEX,MEMBER) (selected[(MEMBER)][(INDEX)]) // obtem um membro selecionado da populacao (fronteira?)
//#define SETREAL(REAL,INDEX,MEMBER) (population[(MEMBER)][(INDEX)] = REAL) //insere um membro na populacao
//#define SETREALSELECTED(REAL,INDEX,MEMBER) (selected[(MEMBER)][(INDEX)] = REAL) //insere um membro selecionado na populacao (fronteira?)
#define SMALLER_VALUE_BETTER(X1,X2) (X1 < X2)

double GETREALSELECTED(int j, int i){
	if(j>= stringlength)
		printf("ERRO!!! stringlength");
	if(i>= swarm[0].repository.getActualSize())
		printf("ERRO!!! selsize (%d)\n", i);
	//return selected[i][j];
	//return 0;
	return swarm[0].repository.getSolution(i).decisionVector[j];
}

double GETREAL(int j, int i){
	if(j>= stringlength)
		printf("ERRO!!! stringlength");
	if(i>= popsize)
		printf("ERRO!!! popsize");
	//return selected[i][j];
	//return 0;
	return swarm[0].particles[i].solution.decisionVector[j];
}
void SETREAL(double real, int j, int i){
	if(j>= stringlength)
		printf("ERRO!!! stringlength");
	if(i>= popsize)
		printf("ERRO!!! popsize");
	//return selected[i][j];
	//return 0;
// 	printf("\na: %f", swarm.particles[i].solution.decisionVector[j]);
// 	printf("\nd: %f", real);
	
	
	swarm[0].particles[i].solution.decisionVector[j]=real;
}

#include "Miscellaneous.h"
#include "Matrix.h"
#include "RandomNumber.h"
#include "rBOABody.h"

void eda(Swarm &sw, int swarmSize, bool init){
	swarm=&sw;
	
	//memcpy(&swarm, &sw, sizeof(Swarm));

	selsize=swarm[0].repository.getActualSize();
	stringlength=decisionNumber;
	popsize=swarmSize;
    
	if(init)
		initRandom();

	clusteringWholeProblem();
	
	modelSelection();
	
	decomposeProblem();
	
	clusteringSubproblems( useClusters, leaderThreshold, maximumAmountOfClusters);
	
	modelFitting();
		
	selsize=0;
	
	generateNewSolutions();
	
	//memcpy(&sw, &swarm, sizeof(Swarm));
	
	//free
	for(int i = 0; i < maxNumClustersWholeProblem; i++ )
		free(wholeClusters[i]);
	free(wholeClusters);
	wholeClusters=0;
	free(wholeClustersSize);

	//free memory of clustering subproblems
	free(clustersUsedOverall);
	free(clustersUsed);
	for(int i = 0; i < stringlength; i++ ){
		for(int j = 0; j < maximumAmountOfClusters; j++ )
			free(clusters[i][j]);

		free(clusters[i]);
		free(clustersSize[i]);
		free(alpha[i]);
	}
	free(clusters);
	clusters=0;
	free(clustersSize);
	free(alpha);
	
// 	printf("\n particle after: ");
// 	printVector(swarm.particles[10].solution.objectiveVector, objectiveNumber);
// 	printf(" -> ");
// 	printVector(swarm.particles[10].solution.decisionVector, decisionNumber);
}