//verify if a vector has any nan
//params - the vector to be verified
//		 - the number of size of the vector
__host__ __device__ bool vectorNan(double* x, int size){
	for( int i = 0; i < size; i++ ){
		if( x[i] != x[i] ){
			return true;
		}
	}

  return false;
}
//print a vector
//params - the vector to be printed
//		 - the number of size of the vector
__device__ __host__ void printVector(double* vec, int vecSize){
	for(int i=0;i<vecSize;i++){
		printf("%f ", vec[i]); //%e
	}
}
//saves a vector to a file
//params - the vector to be printed
//		 - the number of size of the vector
//		 - the file name
__host__ void printVectorToFile(double* vec, int vecSize, char* fileName){
	if(vectorNan(vec, vecSize)){
		printf("\nERROR! NaN on the output file\n");
		exit(1);
	}
	
	FILE * pFile;
	pFile = fopen (fileName,"aw"); //aw append and write
	if (pFile==NULL){
		printf("FILE ERROR\n");
		exit(1);
	}

	for(int i=0;i<vecSize;i++)
		fprintf(pFile, "%e ", vec[i]);
	fprintf(pFile, "\n");

	fclose (pFile);
}
//insert a blank line in the output file
__host__ void insertBlankLine(char* fileName){
	FILE * pFile;
	pFile = fopen (fileName,"aw"); //aw append and write
	if (pFile==NULL){
		printf("FILE ERROR\n");
		exit(1);
	}
	fprintf(pFile, "\n\n");
	fclose (pFile);
}
//clear the file
__host__ void clearFile(char* fileName){
	FILE * pFile;
	pFile = fopen (fileName,"w"); //aw append and write
	if (pFile==NULL){
		printf("FILE ERROR\n");
		exit(1);
	}
	fclose (pFile);
}

//function used in the file reading
void parse( char *record, char *delim, char arr[][1024],int *fldcnt){
    char*p=strtok(record,delim);
    int fld=0;
    
    while(p != NULL)
    {
        strcpy(arr[fld],p);
		fld++;
		p=strtok('\0',delim);
	}		
	*fldcnt=fld;
}

//function to read the files an throw they values to the matrices in the memory
int lerArquivos(double dados[][maxObjectiveNumber], char* arquivo){
	char tmp[4096];
	int fldcnt=0;
	char arr[1000][1024];
	int recordcnt=0;
	FILE *in=fopen(arquivo,"r");         // open file on command line 
	
	if(in==NULL)
	{
		perror("Error opening the file\n");
		exit(EXIT_FAILURE);
	}
	while(fgets(tmp,sizeof(tmp),in)!=0) // read a record 
	{
		parse(tmp,(char*)" \t",arr,&fldcnt);   // whack record into fields
		for(int coluna=0;coluna<fldcnt;coluna++){
			for(int i=0;i<strlen(arr[coluna]);i++){
					if(arr[coluna][i] == ',')
						arr[coluna][i]='.';
			}
			dados[recordcnt][coluna]=(double)atof(arr[coluna]);
		}
		recordcnt++;
	}
    fclose(in);
    return recordcnt;
}
int lerArquivos(double dados[][maxDecisionNumber], char* arquivo){
	char tmp[4096];
	int fldcnt=0;
	char arr[1000][1024];
	int recordcnt=0;
	FILE *in=fopen(arquivo,"r");         // open file on command line 
	
	if(in==NULL)
	{
		perror("Error opening the file");
		exit(EXIT_FAILURE);
	}
	while(fgets(tmp,sizeof(tmp),in)!=0) // read a record 
	{
		parse(tmp,(char*)" \t",arr,&fldcnt);   // whack record into fields
		for(int coluna=0;coluna<fldcnt;coluna++){
			for(int i=0;i<strlen(arr[coluna]);i++){
					if(arr[coluna][i] == ',')
						arr[coluna][i]='.';
			}
			dados[recordcnt][coluna]=(double)atof(arr[coluna]);
		}
		recordcnt++;
	}
    fclose(in);
    return recordcnt;
}

//multiply two vectors
//params - the first vector to be multiplied
//		 - the second vector to be multiplied
//		 - the vector to receive the result
//		 - the length of the vectors (the three vectors should have the same size)
void vectorMultiply(double A[], double B[], double C[], int length){
	for(int i=0;i<length;i++)
		C[i]=A[i]*B[i];
}
//multiply scalar by vector
//params - the scalar to be multiplied
//		 - the vector to be multiplied
//		 - the vector to receive the result
//		 - the length of the vectors (the two vectors should have the same size)
__device__ void scalarVectorMultiply(double scalar, double* vector, double* C, int length){
	for(int i=0;i<length;i++)
		C[i]=scalar*vector[i];
}
/*__device__ void scalarVectorMultiply(double scalar, thrust::device_vector<double> vec, double* C, int length){
	for(int i=0;i<length;i++)
		C[i]=scalar*vec[i];
}
__device__ void scalarVectorMultiply(double scalar, double* vector, thrust::device_vector<double> C, int length){
	for(int i=0;i<length;i++)
		C[i]=scalar*vector[i];
}*/
//sum two vectors
//params - the first vector to be sum
//		 - the second vector to be sum
//		 - the vector to receive the result
//		 - the length of the vectors (the three vectors should have the same size)
__device__ __host__ void vectorSum(double* A, double* B, double* C, int length){
	for(int i=0;i<length;i++){
		C[i]=A[i] + B[i];
	}
}
//subtract two vectors
//params - the first vector to be sum
//		 - the second vector to be sum
//		 - the vector to receive the result
//		 - the length of the vectors (the three vectors should have the same size)
__device__ __host__ void vectorSub(double* A, double* B, double* C, int length){
	for(int i=0;i<length;i++){
		C[i]=A[i] - B[i];
	}
}
/*__device__ __host__ void vectorSum(std::vector<double> A, thrust::device_vector<double> B, thrust::device_vector<double> C, int length, int signal){
	for(int i=0;i<length;i++){
		C[i]=A.at(i) + (B[i]*signal);
	}
}
__device__ __host__ void vectorSum(double* A, thrust::device_vector<double> B, double* C, int length, int signal){
	for(int i=0;i<length;i++){
		C[i]=A[i] + (B[i]*signal);
	}
}*/

//verify the dominance relation between two vectors
//return 1 if sol1 dominates sol2, -1 if sol2 dominates sol1, 0 if they do not dominate each other, 2 if they are equal
//params - the objective vector of the first solution to be compared
//		 - the objective vector of the second solution to be compared
//		 - the number of objectives
// return- the number corresponding to the comparison
// __device__ __host__ int dominance(double sol1[], double sol2[], int objectiveNumber){
// 	int cont=0;
// 	int cont2=objectiveNumber;
// 	for(int i=0;i<objectiveNumber;i++){
// 		if(sol1[i]<sol2[i])
// 			++cont;
// 		else{
// 			if(sol1[i] == sol2[i]){
// 				--cont2;
// 			}
// 		}
// 	}
// 	if(cont==0){
// 		if(cont2==0)
// 			return 2;//EQUALS
// 		else
// 			return -1;//SOL1 DOMINATED BY SOL2
// 	}else{
// 		if(cont>0 && cont<cont2)
// 			return 0;//NON_DOMINATED
// 		else
// 			return 1;//SOL1 DOMINATES SOL2
// 	}
// }
__device__ __host__ double round_5(double in){
	int tmp = round(in*100000);
	return tmp/100000.0;
}
__device__ __host__ int dominance(double sol1[], double sol2[], int objectiveNumber){
	int dominate1 = 0; // dominate1 indicates if some objective of solution1 
				// dominates the same objective in solution2. dominate2
	int dominate2 = 0; // is the complementary of dominate1.    
	int flag; //stores the result of the comparison

	double value1, value2;
	for (int i = 0; i < objectiveNumber; i++) {
		value1 = sol1[i];
		value2 = sol2[i];
		//value1=round_5(sol1[i]);
		//value2=round_5(sol2[i]);
		
		if (value1 < value2) {
			flag = -1;
		} else if (value1 > value2) {
			flag = 1;
		} else {
			flag = 0;
		}
		
		if (flag == -1) {
			dominate1 = 1;
		}
		
		if (flag == 1) {
			dominate2 = 1;           
		}
	}
			
	if (dominate1 == dominate2) {            
		return 0; //No one dominate the other
	}
	if (dominate1 == 1) {
		return 1; // solution1 dominate
	}
	return -1;    // solution2 dominate   
}
//sort a set of solutions according to an objective
//params - the set of solutions to be sorted
//		 - the index of the objective in wich the solutions should be sorted
//		 - the number of solutions to be sorted
__device__ __host__ void shellSort(Solution* solutions, int index, int solutionNumber){
	int i, flag = 1;
	Solution temp;
	//temp.objectiveNumber=solutions[0].objectiveNumber;
	//temp.decisionNumber=solutions[0].decisionNumber;
	int d = solutionNumber;
	while( flag || (d > 1)){  // boolean flag (true when not equal to 0)
	flag = 0; // reset flag to 0 to check for future swaps
	d = (d+1) / 2;
		for (i = 0; i < (solutionNumber - d); i++){
			if (solutions[i+d].objectiveVector[index] < solutions[i].objectiveVector[index]){
			  temp = solutions[i+d]; // swap positions i+d and i
			  solutions[i+d]=solutions[i];
			  solutions[i]=temp;
			  flag = 1; // tells swap has occurred
			}
		}
	}
}
//verify if a solution set is ordered
//params - the set of solutions to verified
//		 - the index of the objective in wich the solutions should be sorted
//		 - the number of solutions to be sorted
__device__ __host__ bool ordered(Solution* solutions, int index, int solutionNumber){
	for(int i=1;i<solutionNumber;i++)
		if(solutions[i-1].objectiveVector[index] > solutions[i].objectiveVector[index])
			return false;
		
		return true;
}

__device__ Solution d_solutions[maxRepositorySize+1];
__device__ double nextSolution(int idx, int obj, int solutionNumber){
	double val=MAXDOUBLE;
	for(int s=0;s<solutionNumber;s++){
		if(d_solutions[s].objectiveVector[obj] >= d_solutions[idx].objectiveVector[obj] && s!=idx){
			if(d_solutions[s].objectiveVector[obj] <= val)
				val=d_solutions[s].objectiveVector[obj];
		}
	}
	return val;
}
__device__ double previousSolution(int idx, int obj, int solutionNumber){
	double val=MAXDOUBLE*-1;
	for(int s=0;s<solutionNumber;s++){
		if(d_solutions[s].objectiveVector[obj] <= d_solutions[idx].objectiveVector[obj] && s!=idx){
			if(d_solutions[s].objectiveVector[obj] >= val)
				val=d_solutions[s].objectiveVector[obj];
		}
	}	
	return val;
}
__global__ void cudaCrowd(int solutionNumber){
	int idx = blockIdx.x*blockDim.x + threadIdx.x;
	if(idx<solutionNumber){
		d_solutions[idx].crowdingDistance=0;
		for(int obj=0;obj<d_objectiveNumber;obj++){
			int objetiveMaxn=0;
			int objetiveMinn=0;
			
			for(int s=0;s<solutionNumber;s++){
				if(d_solutions[s].objectiveVector[obj] > d_solutions[objetiveMaxn].objectiveVector[obj])
					objetiveMaxn=s;
				if(d_solutions[s].objectiveVector[obj] < d_solutions[objetiveMinn].objectiveVector[obj])
					objetiveMinn=s;
			}
			if(objetiveMaxn==idx || objetiveMinn==idx){
				d_solutions[idx].crowdingDistance=MAXDOUBLE;
				break;
			}
			double difference=d_solutions[objetiveMaxn].objectiveVector[obj]-d_solutions[objetiveMinn].objectiveVector[obj];
			double distance=nextSolution(idx, obj,solutionNumber)-previousSolution(idx, obj,solutionNumber);
			if(difference>0)
				distance/=difference;
			else
				distance=0;
			
			d_solutions[idx].crowdingDistance+=distance;
		}
	}
}

//update the crowding distance of the given set of solutions
//params - the set of solutions to update the crowding distances
//		 - the number of solutions to update the crowding distances

__host__ void updateCrowdingDistances(Solution* solutions, int solutionNumber){	
	cudaMemcpyToSymbol(d_solutions, solutions, sizeof(Solution)*solutionNumber, 0, cudaMemcpyHostToDevice);
	cudaCrowd <<< solutionNumber/blockSize + (solutionNumber%blockSize == 0?0:1), blockSize >>> (solutionNumber);
	cudaMemcpyFromSymbol(solutions, d_solutions, sizeof(Solution)*solutionNumber, 0, cudaMemcpyDeviceToHost);
	
	
// 	for(int s=0;s<solutionNumber;s++){
// 		solutions[s].crowdingDistance=0;
// 	}
// 	
// 	double objetiveMaxn;
// 	double objetiveMinn;
// 	double distance;
// 	
// 	for(int obj=0;obj<objectiveNumber;obj++){
// 		
// 		shellSort(solutions, obj, solutionNumber);		
// 		
// 		objetiveMinn=solutions[0].objectiveVector[obj];
// 		solutions[0].crowdingDistance=MAXDOUBLE;
// 		objetiveMaxn=solutions[solutionNumber-1].objectiveVector[obj];
// 		solutions[solutionNumber-1].crowdingDistance=MAXDOUBLE;
// 
// 		for(int s=1;s<solutionNumber-1;s++){
// 			//solutions[s].crowdingDistance+=solutions[s+1].objectiveVector[obj]-solutions[s-1].objectiveVector[obj];
// 			distance=solutions[s+1].objectiveVector[obj]-solutions[s-1].objectiveVector[obj];
// 			distance/=(objetiveMaxn-objetiveMinn);			
// 			solutions[s].crowdingDistance+=distance;
// 		}
// 	}
}
__device__ double calculateNWSum(Solution solution, Solution GBest, double* smallerPositions, double* largerPositions){
	int k;
	double res1=0,resParc=0,NWSum=0;

	for(int objective=0;objective<d_objectiveNumber;objective++){
		res1=0;
		for(k=0;k<d_objectiveNumber;k++){
			//res1+=solution.objectiveVector[k];
			res1+=normalize(solution.objectiveVector[k], smallerPositions[k], largerPositions[k]);
		}
		//resParc=solution.objectiveVector[objective]/res1;
		resParc=normalize(solution.objectiveVector[objective], smallerPositions[objective], largerPositions[objective])/res1;

		//NWSum+=resParc*GBest.objectiveVector[objective];
		NWSum+=resParc*normalize(GBest.objectiveVector[objective], smallerPositions[objective], largerPositions[objective]);
	}
	return NWSum;
}
__device__ __host__ double calculateEuclideanDistance(double* vector1, double* vector2, int size){
	double sum = 0;
	for (int i = 0; i < size; i++) {
		//sum += pow(vector1[i]-vector2[i],2);
		sum+= ((vector1[i]-vector2[i])*(vector1[i]-vector2[i]));
	}
	return sqrt(sum);
}
__device__ __host__ double factorial(int n){
	double fat = 1;
	for(int i = n;i>0;i--){
		fat*=i;
	}
	return fat;
}
__device__ __host__ double combination(int m, int n){
	if(n==m)
		return 1;
	else{
		double fatM = factorial(m);
		double fatN = factorial(n);
		double fatNM = factorial(m-n);
		return (fatM)/(fatN*fatNM);
	}
}
__device__ double calculateSigma(double f1, double f2){
	double value = (f1*f1) - (f2*f2);
	double denominator = (f1*f1)+ (f2*f2);
	if(denominator!=0){
		return  value/denominator;}
	else
		return 0;
}
__host__ int compute_b_mga(Solution* solutions, int repActualSize, double* smallerPositions, double* largerPositions){
	double max_value = 0;
// 	for(int i=0;i<repActualSize;i++){
// 		for(int j = 0; j<objectiveNumber;j++){
// 			double value=normalize(solutions[i].objectiveVector[j], smallerPositions[j], largerPositions[j]);
// // 			double value=solutions[i].objectiveVector[j];
// 			if(value> max_value)
// 				max_value = value;
// 		}
// 	}
	
	//our values are normalized, hence the maximum value possible is 1
	max_value=1;
	
	return (int) floor(log2(max_value)) + 1;
}
__host__ void box_mga(Solution solution, double* output, int b, int objectiveNumber, double* smallerPositions, double* largerPositions){
	for(int i = 0; i<objectiveNumber; i++){
		double z = normalize(solution.objectiveVector[i], smallerPositions[i], largerPositions[i]);
// 		double z=solution.objectiveVector[i];
		
		output[i] = floor(z / pow(2.0, b) + 0.5);
		//output[i]=(int) floor(z*pow(2,-b));
	}
}
//update the larger and smaller vectors of the repository to normalize the solutions
void updateLargerSmallerVectors(Swarm &swarm, int swarmSize){
	//int objectiveNumber=swarm.particles[0].objectiveNumber;
	for(int o=0;o<objectiveNumber;o++){
		swarm.repository.largerPositions[o]=MAXDOUBLE*-1;
		swarm.repository.smallerPositions[o]=MAXDOUBLE;
	}
	//update the larger and smaller positions vectors from the population
	for(int i=0; i < swarmSize; i++){
		for(int o=0;o<objectiveNumber;o++){
			if(swarm.particles[i].solution.objectiveVector[o] > swarm.repository.largerPositions[o])
				swarm.repository.largerPositions[o]=swarm.particles[i].solution.objectiveVector[o];
			if(swarm.particles[i].solution.objectiveVector[o] < swarm.repository.smallerPositions[o])
				swarm.repository.smallerPositions[o]=swarm.particles[i].solution.objectiveVector[o];
		}
	}

	//update the larger and smaller positions vectors from the repository
	for(int i=0; i< swarm.repository.getActualSize(); i++){
		for(int o=0;o<objectiveNumber;o++){
			if(swarm.repository.getSolution(i).objectiveVector[o] > swarm.repository.largerPositions[o])
				swarm.repository.largerPositions[o]=swarm.repository.getSolution(i).objectiveVector[o];
			if(swarm.repository.getSolution(i).objectiveVector[o] < swarm.repository.smallerPositions[o])
				swarm.repository.smallerPositions[o]=swarm.repository.getSolution(i).objectiveVector[o];
		}
	}
}
//update the larger and smaller vectors of the repository in relation to a new solution
void updateLargerSmallerVectors(Swarm &swarm, Solution sol){
	for(int o=0;o<objectiveNumber;o++){
		if(sol.objectiveVector[o] > swarm.repository.largerPositions[o])
			swarm.repository.largerPositions[o]=sol.objectiveVector[o];
		if(sol.objectiveVector[o] < swarm.repository.smallerPositions[o])
			swarm.repository.smallerPositions[o]=sol.objectiveVector[o];
	}
}
void calculateLargerSmallerVectors(Repository &rep1, Repository &rep2, double reference[][maxObjectiveNumber], int tamRef, double* smallerPositions, double* largerPositions){
	for(int o=0;o<objectiveNumber;o++){
		largerPositions[o]=MAXDOUBLE*-1;
		smallerPositions[o]=MAXDOUBLE;
	}
	//update the larger and smaller positions vectors from rep1
	for(int i=0; i< rep1.getActualSize(); i++){
		for(int o=0;o<objectiveNumber;o++){
			if(rep1.getSolution(i).objectiveVector[o] > largerPositions[o])
				largerPositions[o]=rep1.getSolution(i).objectiveVector[o];
			if(rep1.getSolution(i).objectiveVector[o] < smallerPositions[o])
				smallerPositions[o]=rep1.getSolution(i).objectiveVector[o];
		}
	}
	//update the larger and smaller positions vectors from rep2
	for(int i=0; i< rep2.getActualSize(); i++){
		for(int o=0;o<objectiveNumber;o++){
			if(rep2.getSolution(i).objectiveVector[o] > largerPositions[o])
				largerPositions[o]=rep2.getSolution(i).objectiveVector[o];
			if(rep2.getSolution(i).objectiveVector[o] < smallerPositions[o])
				smallerPositions[o]=rep2.getSolution(i).objectiveVector[o];
		}
	}
	//update the larger and smaller positions vectors from reference
	for(int i=0; i< tamRef; i++){
		for(int o=0;o<objectiveNumber;o++){
			if(reference[i][o] > largerPositions[o])
				largerPositions[o]=reference[i][o];
			if(reference[i][o] < smallerPositions[o])
				smallerPositions[o]=reference[i][o];
		}
	}
}
__device__ int cudaStrncmp(char* str1, char* str2, int size){
	for(int i=0;i<size;i++)
		if( (unsigned int)str1[i] !=  (unsigned int)str2[i])
			return 1;
		
		return 0;	
}
__host__ void KMeans(Solution* solutions, int solNumber, Swarm* multiswarm, int swarmNumber){ // swarm

	int maxIter=50; //max iterations to find the right centroids
	int numClusters=swarmNumber;
	int selsize=solNumber;
	int stringlength=decisionNumber;
	double newCentroid[numClusters][stringlength], Distances[numClusters];
	int wholeClusters[numClusters][selsize], wholeClustersSize[numClusters], init_central_pos[numClusters];
	bool flag = true;

    // Randomly Choose the new central point.
    init_central_pos[0] = rand()%selsize; //RANDOMNUMBER( selsize );
    for(int i=1; i<numClusters; i++ ) {
		flag = true;
		while(flag) {
			flag = false;
			init_central_pos[i] = rand()%selsize;//RANDOMNUMBER( selsize );

			for(int j=0; j<i; j++ ) {
				if( init_central_pos[i] == init_central_pos[j] )
					flag = true;
				}
		}
    }
    ///////// Periodically Choose ////////////
    for(int i=0; i<numClusters; i++ ) {
		for(int j=0; j<stringlength; j++ ) {
			newCentroid[i][j] = solutions[init_central_pos[i]].decisionVector[j];//  solutions[init_central_pos[i]][j];
		}
    }

    /////////////////////////////////////////////////////////////////////////
    ////////////////////  Start K-means Clustering  /////////////////////////
    /////////////////////////////////////////////////////////////////////////
    flag = true;
    int Iter = 0;
    while( flag ) {

		/*** initialization of wholeClusters & wholeClustersSize ***/
		for(int i=0; i<numClusters; i++ ) {
			wholeClustersSize[i] = 0;
			for(int j=0; j<selsize; j++ )
				wholeClusters[i][j] = 0;
		}
		/**************** End of the Initilization ****************/

		for(int i=0; i<selsize; i++ ) {
			for(int j=0; j<numClusters; j++ ) { //euclidean distance
				double value;
				value = 0.0; Distances[j] = 0.0;
				for( int k=0; k<stringlength; k++ ) {
					value = newCentroid[j][k] - solutions[i].decisionVector[k];//solutions[i][k];//GETREALSELECTED( k, i );
					Distances[j] += (value*value);
				}
			}
			int cluster_id=0;
			double min = Distances[0];
			for( int x=1; x<numClusters; x++ ) {
				if( Distances[x] < min ) {
					min = Distances[x];
					cluster_id = x;
				}
			}

			wholeClusters[ cluster_id ][ wholeClustersSize[cluster_id] ] = i;
			wholeClustersSize[cluster_id]++;
		}
		for(int i=0; i<numClusters; i++ ) {
			for(int j=0; j<stringlength; j++ ) {
				newCentroid[i][j] = 0;

				for( int x=0; x<wholeClustersSize[i]; x++ ) {
					newCentroid[i][j] += solutions[wholeClusters[i][x]].decisionVector[j];//solutions[wholeClusters[i][x]][j];// GETREALSELECTED( j, wholeClusters[i][x] );
				}
				newCentroid[i][j] = newCentroid[i][j]/(double)wholeClustersSize[i];
			}
		}
		Iter++;

		if( Iter > maxIter ) flag = false;
    }
    for(int i=0; i<numClusters; i++ ){
    	for(int c=0;c<wholeClustersSize[i];c++){
			updateLargerSmallerVectors(multiswarm[i], solutions[wholeClusters[i][c]]); //update the larger and smaller vectors to normalize in the mga archiver
			multiswarm[i].repository.add(solutions[wholeClusters[i][c] ]); //tries to insert the solutions in the repository
    	}
    	multiswarm[i].repository.organize();
    	for(int s=0; s<stringlength; s++ )
    		multiswarm[i].centroid[s]=newCentroid[i][s];
    }

    /*for(int i=0; i<numClusters; i++ ){
    	for(int s=0; s<stringlength; s++ )
    			printf("%.3f  ", newCentroid[i][s]);
    	printf("(%d)\n", wholeClustersSize[i]);
	for(int c=0;c<wholeClustersSize[i];c++){
	    for(int j=0;j<stringlength;j++)
		printf("%.3f  ", swarm.repository.getSolution(wholeClusters[i][c]).decisionVector[j]);  //solutions[wholeClusters[i][c]][j]);
	    printf("\n");
	}
	printf("\n\n\n");
    }*/
}
//find the extreme solutions and the solution closer to the ideal
//params - solutions of the front
//		 - extreme solutions (output)
//		 - the number of objectives
// void obtainIdealSolution(Solution* solutions, Solution ideal, int objectiveNumber, int repActualSize){
// // 	for(int i=0;i<objectiveNumber;i++)
// // 		ideal.objectiveVector[i]=MAXDOUBLE;
// 	//Obtain the extreme solutions and calculates the ideal
// 	for(int i=0;i<repActualSize;i++){
// 		for(int j = 0; j<objectiveNumber;j++){
// 			if(solutions[i].objectiveVector[j]<=ideal.objectiveVector[j]){
// 				ideal.objectiveVector[j] = solutions[i].objectiveVector[j];
// 			}
// 		}
// 	}
// }

double r2_max(double* V, double* A, int objectiveNumber, double* smallerPositions, double* largerPositions){
	double max=(MAXDOUBLE*-1);
	for(int j=0;j<objectiveNumber;j++){
		//double valor=V[j]*fabs(ponto[j]-A[j]);
		//double valor=V[j]*fabs(0-A[j]); //considering the origin as reference point
		double valor= normalize(V[j], smallerPositions[j], largerPositions[j])*normalize(A[j], smallerPositions[j], largerPositions[j]);
		if( valor > max)
			max=valor;
	}
	return max;
}
double r2_min(double* V, Repository &rep, double* smallerPositions, double* largerPositions){
	double min=MAXDOUBLE;
	for(int i=0;i<rep.getActualSize();i++){
		double valor=r2_max(V, rep.getSolution(i).objectiveVector, objectiveNumber, smallerPositions, largerPositions);
		if(valor < min)
			min=valor;
	}
	return min;
}
//double calcularR2(Solution* front, int tamFront, int objectiveNumber, double referencia[][maxObjectiveNumber+1], int tamRef){
double calcularR2(Repository &rep, double* smallerPositions, double* largerPositions, double referencia[][maxObjectiveNumber], int tamRef){
	double R2=0;
	for(int i=0;i<tamRef;i++){
		R2+=r2_min(referencia[i], rep, smallerPositions, largerPositions);
	}
	R2*=(1.0/tamRef);
	return R2;
}
__host__ __device__ double normalize(double valor, double min, double max){
	double diff=max-min;
	if(diff > 0){
		//printf("antes: %f, depois: %f\n", valor, ((valor-min)/(max-min)));
		return ((valor-min)/diff);
	}else{
		if(valor > 0 && (max != min))
			printf("min: %f, max: %f, valor: %f\n", min, max, valor);
		return (valor-min);//((valor-min)/(max-min));
	}
	//return valor;
	//normalize(h_swarm[0].particles[i].solution.objectiveVector[p], h_swarm[0].repository.smallerPositions[p], h_swarm[0].repository.largerPositions[p])
}
//comparator if two vectors are equal
//param - the vectors to be compared to
__host__ __device__ bool isEqual(double* vec1, double* vec2, int size){
	for(int i=0;i<size;i++){
		if(vec1[i] != vec2[i])
			return false;
	}
	return true;
}